package objectsTraining;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PessoaTest {

    //2.Determine se duas pessoas têm a mesma data de nascimento.

    @Test
    @DisplayName("Determinar que duas pessoas têm a mesma data de nascimento - happy case")
    void mesmaDataDeNascimento() {
        //Arrange
        boolean expected = true;
        boolean result;

        //Act
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "01-01-1990",null, null );
        result = p1.mesmaDataDeNascimento(p2);


        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Determinar se duas pessoas não têm a mesma data de nascimento")
    void naoTemMesmaDataDeNascimento() {
        //Arrange
        boolean expected = false;
        boolean result;

        //Act
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "31-12-1980", null, null );
        result = p1.mesmaDataDeNascimento(p2);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Determinar se duas pessoas não têm a mesma data de nascimento - os campos sao null")
    void naoTemMesmaDataDeNascimentoNull () {
        //Arrange
        boolean expected = false;
        boolean result;

        //Act
        Pessoa p1 = new Pessoa("Ana", null, null, null );
        Pessoa p2 = new Pessoa("Joana", null, null, null );
        result = p1.mesmaDataDeNascimento(p2);

        //Assert
        assertEquals(expected, result);
    }

    //3. Determine se duas pessoas têm o mesmo nome.

    @Test
    @DisplayName("Determinar se duas pessoas não têm o mesmo nome")
    void naoTemMesmoNome() {
        //Arrange
        boolean expected = false;
        boolean result;

        //Act
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "01-01-1990", null, null );
        result = p1.mesmoNome(p2);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Determinar se duas pessoas têm o mesmo nome - happy case")
    void mesmoNome() {
        //Arrange
        boolean expected = true;
        boolean result;

        //Act
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Ana", "01-01-1990", null, null );
        result = p1.mesmoNome(p2);

        //Assert
        assertEquals(expected, result);
    }

    //4 e 5. Determinar se duas pessoas têm o mesmo local de nascimento

    @Test
    @DisplayName("Determinar se duas pessoas têm o mesmo local de nascimento - happy case")
    void mesmoLocalNascimento() {
        //Arrange
        boolean expected = true;
        boolean result;

        //Act
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        p1.setLocalDeNascimento("Fafe");
        Pessoa p2 = new Pessoa("Ana", "01-01-1990", null, null );
        p2.setLocalDeNascimento("Fafe");
        result = p1.mesmoLocalNascimento(p2);

        //Assert
        assertEquals(expected, result);
    }

    //6. Adicione um irmão.

    @Test
    @DisplayName("Adicione um irmão - happy case")
    void adicionarIrmao () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );
        Pessoa [] expected = {p2};

        //Act
        p1.adicionarIrmao(p2);
        Pessoa [] result = p1.getIrmaos();

        //Assert
        assertArrayEquals(expected, result);
    }

    @Test
    @DisplayName("Adicionar irmao e comparar com pessoa que não é irmao")
    void adicionarIrmaoComparar () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );
        Pessoa p3 = new Pessoa("Pedro", "01-01-1992", null, null );
        Pessoa [] expected = {p3};

        //Act
        p1.adicionarIrmao(p2);
        Pessoa [] result = p1.getIrmaos();

        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    @DisplayName("Adicionar 60 irmaos - verificar se todos os irmaos foram adicionados")
    void adicionarVáriosIrmao () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        //Act
        //Add siblings to Ana
        for (int i = 0; i < 61; i++) {
            p1.adicionarIrmao(p2);
        }

        Pessoa [] irmaosAna = p1.getIrmaos();
        boolean valid = true;
        for (int i = 0; i < 61; i++) {
            if (irmaosAna[i] != p2) {
                valid = false;
            }
        }

        //Assert
        assertTrue(valid);
    }

    //7. Retire um irmao

    @Test
    @DisplayName("Retire um irmão - happy case")
    void retirarIrmao () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );
        Pessoa p3 = new Pessoa("Maria", "06-06-2000", null, null );
        Pessoa p4 = new Pessoa ("Mafalda", "03-10-2005", null, null );
        Pessoa [] expected = {p3, p4};

        //Act
        p1.adicionarIrmao(p2);
        p1.adicionarIrmao(p3);
        p1.adicionarIrmao(p4);
        p1.retirarIrmao(p2);
        Pessoa [] result = p1.getIrmaos();

        //Assert
        assertArrayEquals(expected, result);
    }

    //8. Determine se duas pessoas têm os mesmos irmãos.

    @Test
    @DisplayName("mesmosIrmaos - Ambas as pessoas têm os mesmo irmãos na mesma ordem")
    void mesmosIrmaosMesmaOrdem () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(p2);
        p2.adicionarIrmao(p1);
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);
        p2.adicionarIrmao(s2);

        //Assert
        assertTrue(p1.mesmosIrmaos(p2));
    }

    @Test
    @DisplayName("mesmosIrmaos - Ambas as pessoas têm os mesmo irmãos mas em ordens diferentes")
    void mesmosIrmaosOrdemDiferente () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(p2);
        p2.adicionarIrmao(p1);
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        assertTrue(p1.mesmosIrmaos(p2));
    }


    @Test
    @DisplayName("mesmosIrmaos - array Irmaos com número de irmaos diferentes")
    void mesmoIrmaosQuantidadeDiferenteIrmaos () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null);

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null);
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null);

        // Add siblings
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        assertFalse(p1.mesmosIrmaos(p2));
    }

    @Test
    @DisplayName("mesmosIrmaos - irmaos diferentes")
    void mesmoIrmaosIrmaosDiferentes () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null);

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null);
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null);

        // Add siblings
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        assertFalse(p1.mesmosIrmaos(p2));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    @Test
    @DisplayName("verificarSeMesmosIrmaos - Both people have the same siblings in the same order")
    void verificarSeMesmosIrmaos () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);
        p2.adicionarIrmao(s2);

        //Assert
        assertTrue(p1.verificarSeMesmosIrmaos2(p2));
    }

    @Test
    @DisplayName("verificarSeMesmosIrmaos - Both persons have the same siblings in different order")
    void verificarSeMesmosIrmaosOrdemDiferente () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        //assertTrue(p1.verificarSeMesmosIrmaos2(p2));
    }

    @Test
    @DisplayName("verificarSeMesmosIrmaos - Persons have different siblings")
    void verificarSeNaoSaoMesmosIrmaos () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s2);

        //Assert
        assertFalse(p1.verificarSeMesmosIrmaos2(p2));
    }

    //9 e 10. Determinar se duas pessoas são irmãos ou meio-irmãos.

    @Test
    @DisplayName("Determinar se duas pessoas são irmãos ou meio-irmãos - sao meios irmaos")
    void irmaosOuMeiosIrmaoSaoMeiosIrmaos () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990",  null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Mafalda", "01-01-1990",  null, null);
        Pessoa p4 = new Pessoa("Joaquim", "02-05-1980",  p1, p2);
        Pessoa p5 = new Pessoa("Maria", "06-06-200", p3, p1);
        Pessoa p6 = new Pessoa("Joao", "03-10-2005", p3, p3);
        String expected = "Sao meios-irmaos.";
        String result;

        //Act
        result = p5.irmaosOuMeiosIrmao(p6);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Determinar se duas pessoas são irmãos ou meio-irmãos - sao irmaos")
    void irmaosOuMeiosIrmaoSaoIrmaos () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990",  null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Mafalda", "01-01-1990",  null, null);
        Pessoa p4 = new Pessoa("Joaquim", "02-05-1980",  p1, p2);
        Pessoa p5 = new Pessoa("Maria", "06-06-200", p3, p4);
        Pessoa p6 = new Pessoa("Joao", "03-10-2005", p3, p4);
        String expected = "Sao irmaos.";
        String result;

        //Act
        result = p5.irmaosOuMeiosIrmao(p6);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Determinar se duas pessoas são irmãos ou meio-irmãos - não sao irmaos")
    void irmaosOuMeiosIrmaoNSaoIrmaos () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990",  null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Mafalda", "01-01-1990",  null, null);
        Pessoa p4 = new Pessoa("Joaquim", "02-05-1980",  null, null);
        Pessoa p5 = new Pessoa("Maria", "06-06-200", p1, p2);
        Pessoa p6 = new Pessoa("Joao", "03-10-2005", p3, p4);
        String expected = "Nao sao irmaos.";
        String result;

        //Act
        result = p5.irmaosOuMeiosIrmao(p6);

        //Assert
        assertEquals(expected, result);
    }

    //11. Determinar se duas Pessoa representam a mesma pessoa

    @Test
    @DisplayName("Duas Pessoa não representam a mesma pessoa")
    void naoMesmaPessoaEquals () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Maria", "01-01-1992", p1,p2);
        Pessoa p4 = new Pessoa("Alberto", "05-05-2000", p1, p2);

        //Act
        p3.setLocalDeNascimento("Porto");
        p4.setLocalDeNascimento("Porto");

        //Assert
        assertNotEquals(p3, p4);
    }

    @Test
    @DisplayName("Duas Pessoa representam a mesma pessoa:")
    void mesmaPessoaEquals () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Maria", "01-01-1992", p1,p2);
        Pessoa p4 = new Pessoa("Maria", "01-01-1992", p1, p2);

        //Act
        p3.setLocalDeNascimento("Porto");
        p4.setLocalDeNascimento("Porto");

        //Assert
        assertEquals(p3, p4);
    }

    @Test
    @DisplayName("Duas Pessoa não representam a mesma pessoa - com alguns campos nulos")
    void naoMesmaPessoaNull () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "05-07-1950", null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1955",  null, null);
        Pessoa p3 = new Pessoa("Maria", "01-01-1992", null,p2);
        Pessoa p4 = new Pessoa("Maria", "01-01-1992", p1, null);

        //Act
        p3.setLocalDeNascimento("Porto");
        p4.setLocalDeNascimento("Porto");

        //Assert
        assertNotEquals(p3, p4);
    }

    @Test
    @DisplayName("Duas Pessoa não representam a mesma pessoa")
    void naoMesmaPessoa () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Maria", "01-01-1992", p1,p2);
        Pessoa p4 = new Pessoa("Alberto", "05-05-2000", p1, p2);
        boolean expected = false;

        //Act
        p3.setLocalDeNascimento("Porto");
        p4.setLocalDeNascimento("Porto");
        boolean result = p3.mesmaPessoa(p4);


        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Duas Pessoa representam a mesma pessoa:")
    void mesmaPessoa () {
        //Arrange
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Pedro", "02-05-1980",  null, null);
        Pessoa p3 = new Pessoa("Maria", "01-01-1992", p1, p2);
        Pessoa p4 = new Pessoa("Maria", "01-01-1992", p1, p2);
        boolean expected = true;

        //Act
        p3.setLocalDeNascimento("Porto");
        p4.setLocalDeNascimento("Porto");
        boolean result = p3.mesmaPessoa(p4);


        //Assert
        assertEquals(expected, result);
    }

}
