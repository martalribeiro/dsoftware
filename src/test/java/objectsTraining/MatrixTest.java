package objectsTraining;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    @DisplayName("Test firstElement() - happy case")
    public void ensureFirstElementIsOne() {
        //Arrange
        int expectedResult = 1;
        int realResult;
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix matrix = new Matrix(defaultMatrix);
        realResult = matrix.getFirstElement();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    //testar método equals
    @Test
    @DisplayName("Test equals() - equal matrices")
    public void ensureTwoMatrixAreEqual() {
        //Arrange
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix firstMatrix = new Matrix(defaultMatrix);
        Matrix secondMatrix = new Matrix(defaultMatrix);

        //Assert
        assertEquals(firstMatrix, secondMatrix);
    }

    @Test
    @DisplayName("Test equals() - 2 equal matrices")
    public void ensureTwoGivenMatrixAreEqual() {
        //Arrange
        int[][] defaultMatrix1 = {{1, 2}, {3, 4}};
        int[][] defaultMatrix2 = {{1, 2}, {3, 4}};

        //Act
        Matrix firstMatrix = new Matrix(defaultMatrix1);
        Matrix secondMatrix = new Matrix(defaultMatrix2);

        //Assert
        assertEquals(firstMatrix, secondMatrix);
    }

    //Finds the matrix highest value
    @Test
    @DisplayName("Test highest value - happy case")
    public void ensureHighestValue() {
        //Arrange
        int expectedResult = 4 ;
        int realResult;
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.highestValue();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test lowest value - happy case")
    public void ensureLowestValue() {
        //Arrange
        int expectedResult =  1;
        int realResult;
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.lowestValue();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test somaLinhas - happy case")
    public void somaLinhas() {
        //Arrange
        int [] expectedResult = {3,7};
        int [] realResult;
        int [][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.somaLinhas();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test somaColunas - happy case")
    public void somaColunas() {
        //Arrange
        int [] expectedResult = {4,6};
        int [] realResult;
        int [][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.somaColunas();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test indiceMaiorSoma - happy case")
    public void indiceMaiorSoma() {
        //Arrange
        int expectedResult = 1 ;
        int realResult;
        int[][] defaultMatrix = {{1, 2, 3}, {4, 5, 6}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.indiceMaiorSoma();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test indiceMaiorSoma - matriz com negativos e nulos")
    public void indiceMaiorSomaNegativosNulos() {
        //Arrange
        int expectedResult = 2 ;
        int realResult;
        int[][] defaultMatrix = {{1, -2, 0}, {-4, 5, 0}, {2, 4, 9}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.indiceMaiorSoma();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test indiceMaiorSoma - linha com maior soma na primeira linha")
    public void indiceMaiorSomaLinha1() {
        //Arrange
        int expectedResult = 0 ;
        int realResult;
        int[][] defaultMatrix = {{2, 4, 9}, {-4, 5, 0}, {-2, -4, -9}, {1, 2, 3}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.indiceMaiorSoma();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test indiceMaiorSoma - matriz com todos os elementos negativos")
    public void indiceMaiorSomaTodosNegativos() {
        //Arrange
        int expectedResult = 1 ;
        int realResult;
        int[][] defaultMatrix = {{-2, -4, -9}, {-4, -5, -3}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.indiceMaiorSoma();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test indiceMaiorSoma - matriz vazia")
    public void indiceMaiorSomaVectorVazio() {
        //Arrange
        int expectedResult = -1 ;
        int realResult;
        int[][] defaultMatrix = {};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.indiceMaiorSoma();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test isSquareMatrix - happy case")
    public void isSquareMatrixTrue() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int[][] defaultMatrix = {{1, 2}, {1, 3}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.isSquareMatrix();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test isSquareMatrix - matriz não quadrada")
    public void isSquareMatrix() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {{1, 2, 3}, {1, 3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.isSquareMatrix();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test isSquareMatrix - matriz vazia")
    public void isSquareMatrixVazia() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.isSquareMatrix();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test matrizQuadSim - happy case")
    public void matrizQuadSim() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int[][] defaultMatrix = {{1, 2}, {2, 1}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.matrizQuadSim();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test matrizQuadSim - 3x3")
    public void matrizQuadSim3x3() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int[][] defaultMatrix = {{3, 2, 1}, {2, 3, 4}, {1, 4, 3}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.matrizQuadSim();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test matrizQuadSim - não simetrica")
    public void matrizQuadNotSim() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {{3, 8, 1}, {4, 3, 4}, {1, 7, 5}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.matrizQuadSim();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test matrizQuadSim - matriz com nulos e negativos")
    public void matrizQuadSimNulosNegativos() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int[][] defaultMatrix = {{3, -8, 0}, {-8, 2,-4}, {0, -4, 5}};

         //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.matrizQuadSim();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test matrizQuadSim - não quadrada")
    public void matrizQuadNotSimNoQuad () {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {{3, 8, 1}, {4, 3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.matrizQuadSim();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test matrizQuadSim - matriz vazia")
    public void matrizQuadMatrizVazia () {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.matrizQuadSim();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test naoNulosDiagPrincQuadrada - happy case")
    public void naoNulosDiagPrincQuadrada() {
        //Arrange
        int expectedResult = 1;
        int realResult;
        int[][] defaultMatrix = {{1, 2}, {3, 0}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.naoNulosDiagPrincQuadrada();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test naoNulosDiagPrincQuadrada - sem nulos")
    public void naoNulosDiagPrincQuadradaSemNulos() {
        //Arrange
        int expectedResult = 2;
        int realResult;
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.naoNulosDiagPrincQuadrada();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test naoNulosDiagPrincQuadrada - matriz não quadrada")
    public void naoNulosDiagPrincQuadradaMatrizNQuad() {
        //Arrange
        int expectedResult = -1;
        int realResult;
        int[][] defaultMatrix = {{1, 2, 3}, {3, 4, 2}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.naoNulosDiagPrincQuadrada();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test princIgualSecund - happy case")
    public void princIgualSecund() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int[][] defaultMatrix = {{2, 1, 2}, {4, 1, 3}, {5, 0, 5}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.princIgualSecund();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test princIgualSecund - não são iguais")
    public void princIgualSecundNaoSaoIguais() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {{3, 2, 1}, {2, 3, 4}, {1, 4, 3}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.princIgualSecund();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test princIgualSecund - não é quadrada")
    public void princIgualSecundNaoSaoIguaisNQuad() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int[][] defaultMatrix = {{3, 2, 1}, {2, 3, 4}};

        //Act
        Matrix number1 = new Matrix(defaultMatrix);
        realResult = number1.princIgualSecund();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar um vetor com todos os elementos da matriz cujo número de algarismos é superior número médio de algarismos" +
            "de todos os elementos da matriz - happy case")
    void mediaSuperiorMediaAlgarismos() {
        //Arrange
        int [] expectedResult = {10,12,123};
        int [] realResult;
        int [][] defaultMatrix = {{1, 2},{3, 10},{12, 123}};

        //Act
        Matrix vector1 = new Matrix(defaultMatrix);
        realResult = vector1.mediaSuperiorMediaAlgarismos();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar um vetor com todos os elementos da matriz cujo número de algarismos é superior número médio de algarismos" +
            "de todos os elementos da matriz - matriz com nulos e negativos")
    void mediaSuperiorMediaAlgarismosNulosNegativos() {
        //Arrange
        int [] expectedResult = {14,50,-190};
        int [] realResult;
        int [][] defaultMatrix = {{14,-2},{50,0},{-190,-6}};

        //Act
        Matrix vector1 = new Matrix(defaultMatrix);
        realResult = vector1.mediaSuperiorMediaAlgarismos();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar um vetor com todos os elementos da matriz cujo número de algarismos é superior número médio de algarismos" +
            "de todos os elementos da matriz - matriz com media superior a 2 algarismos por número")
    void mediaSuperiorMediaAlgarismosSuperior2Algarismos() {
        //Arrange
        int [] expectedResult = {6547,8754,8563};
        int [] realResult;
        int [][] defaultMatrix = {{14, -2},{50, 6547},{68, 46},{8754, 8563}};

        //Act
        Matrix vector1 = new Matrix(defaultMatrix);
        realResult = vector1.mediaSuperiorMediaAlgarismos();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar um vetor com todos os elementos da matriz cujo número de algarismos é superior número médio de algarismos" +
            "de todos os elementos da matriz - matriz vazia")
    void mediaSuperiorMediaAlgarismosMatrizVazia() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [][] defaultMatrix = {};

        //Act
        Matrix vector1 = new Matrix(defaultMatrix);
        realResult = vector1.mediaSuperiorMediaAlgarismos();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - happy case")
    void percentParesSuperiorMedia() {
        //Arrange
        int [] expectedResult = {2,4};
        int [] realResult;
        int [][] defaultMatrix = {{1, 2},{3, 4}};

        //Act
        Matrix vector1 = new Matrix(defaultMatrix);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - negativos e nulos")
    void percentParesSuperiorMediaNegativosNulos() {
        //Arrange
        int [] expectedResult = {-2, -232, 0, 4445};
        int [] realResult;
        int [][] defaultMatrix = {{11, -2, 32},{-232, 323, 0},{4579, -8954, 4445}};

        //Act
        Matrix vector1 = new Matrix(defaultMatrix);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - matrix vazia")
    void percentParesSuperiorMediaEmptyMatrix() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [][] defaultArray = {};

        //Act
        Matrix vector1 = new Matrix(defaultArray);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - percentagem de algarismos pares todos iguais à media")
    void percentParesSuperiorMediaTodosIguaisAMedia() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [][] defaultArray = {{0,2},{4,6},{8,0}};

        //Act
        Matrix vector1 = new Matrix(defaultArray);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyBy2 - happy case")
    public void multiplyBy2() {
        //Arrange
        int [][] matrizMultiplicada = {{2, 4}, {6, 8}};
        int [][] defaultMatrix1 = {{1, 2}, {3, 4}};

        //Act
        Matrix expectedResult = new Matrix(matrizMultiplicada);
        Matrix realResult = new Matrix(defaultMatrix1);
        realResult.multiplyBy(2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test somaMatrizes - happy case")
    public void somaMatrizes() {
        //Arrange
        int [][] somaMatrizesEsperada = {{2, 4}, {6, 8}};
        int [][] defaultMatrix1 = {{1, 2}, {3, 4}};
        int [][] defaultMatrix2 = {{1, 2}, {3, 4}};

        //Act
        Matrix expectedResult = new Matrix(somaMatrizesEsperada);
        Matrix realResult = new Matrix(defaultMatrix1);
        realResult.somaMatrizes(defaultMatrix2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test produtoMatrizes - happy case")
    public void produtoMatrizes() {
        //Arrange
        int [][] produtoMatrizesEsperada = {{7, 7}, {13, 17}};
        int [][] defaultMatrix1 = {{1, 2}, {3, 4}};
        int [][] defaultMatrix2 = {{-1, 3}, {4, 2}};

        //Act
        Matrix expectedResult = new Matrix(produtoMatrizesEsperada);
        Matrix realResult = new Matrix(defaultMatrix1);
        realResult.produtoMatrizes(defaultMatrix2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test invertLineOrder - happy case")
    void invertLineOrder () {
        //Arrange
        int [][] ordemInvertidaEsperada = {{1, 2},{3, 4}};
        int [][] defaultArray2 = {{2, 1},{4, 3}};

        //Act
        Matrix expectedResult = new Matrix(ordemInvertidaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.invertLineOrder();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test invertColumnOrder - happy case")
    void invertColumnOrder () {
        //Arrange
        int [][] ordemInvertidaEsperada = {{4, 3},{2, 1}};
        int [][] defaultArray2 = {{2, 1},{4, 3}};

        //Act
        Matrix expectedResult = new Matrix(ordemInvertidaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.invertColumnOrder();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate90Degrees - happy case")
    void rotate90Degrees () {
        //Arrange
        int [][] matrizRodadaEsperada = {{3, 1},{4, 2}};
        int [][] defaultArray2 = {{1, 2},{3, 4}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate90Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate90Degrees - matriz com 3 linhas e 3 colunas")
    void rotate90Degrees3x3 () {
        //Arrange
        int [][] matrizRodadaEsperada = {{7, 4, 1},{8, 5, 2},{9, 6, 3}};
        int [][] defaultArray2 = {{1, 2, 3},{4, 5, 6},{7, 8, 9}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate90Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rodarMatriz90Graus - não quadrada linhas superior às colunas")
    void rodarMatriz90GrausNQuadradLinhasSupColunas () {
        //Arrange
        int [][] matrizRodadaEsperada = {{5, 3, 1},{6, 4, 2}};
        int [][] defaultArray2 = {{1, 2},{3,4},{5,6}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate90Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rodarMatriz90Graus - não quadrada colunas superior às linhas")
    void rodarMatriz90GrausNQuadradaColunasSupLinhas () {
        //Arrange
        int [][] matrizRodadaEsperada = {{9, 6, 5},{10, 4, 3},{11, 2, 1},{12, 8, 7}};
        int [][] defaultArray2 = {{5, 3, 1, 7},{6, 4, 2, 8}, {9, 10, 11, 12}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate90Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate90Degrees - matriz vazia")
    void rotate90DegreesEmptyMatrix() {
        //Arrange
        int [][] matrizRodadaEsperada = {{},{}};
        int [][] defaultArray2 = {{},{}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate90Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate180Degrees - happy case")
    void rotate180Degrees () {
        //Arrange
        int [][] matrizRodadaEsperada = {{4, 3},{2, 1}};
        int [][] defaultArray2 = {{1, 2},{3, 4}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate180Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate180Degrees - matriz com 3 linhas e 3 colunas")
    void rotate180Degrees3x3 () {
        //Arrange
        int [][] matrizRodadaEsperada = {{9, 8, 7},{6, 5, 4},{3, 2, 1}};
        int [][] defaultArray2 = {{1, 2, 3},{4, 5, 6},{7, 8, 9}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate180Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate180Degrees - matriz não quadrada")
    void rotate180DegreesNotSquare () {
        //Arrange
        int [][] matrizRodadaEsperada = {{6, 5}, {4, 3}, {2, 1}};
        int [][] defaultArray2 = {{1, 2}, {3, 4}, {5, 6}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate180Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotate180Degrees - matriz vazia")
    void rotate180DegreesEmptyMatrix() {
        //Arrange
        int [][] matrizRodadaEsperada = {{},{}};
        int [][] defaultArray2 = {{},{}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotate180Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test rotateNeg90Degrees - happy case")
    void rotateNeg90Degrees () {
        //Arrange
        int [][] matrizRodadaEsperada = {{2, 4},{1, 3}};
        int [][] defaultArray2 = {{1, 2},{3, 4}};

        //Act
        Matrix expectedResult = new Matrix(matrizRodadaEsperada);
        Matrix realResult = new Matrix(defaultArray2);
        realResult.rotateNeg90Degrees();

        //Assert
        assertEquals(expectedResult, realResult);
    }
}