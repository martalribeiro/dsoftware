package objectsTraining;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTest {

    @Test
    @DisplayName("Retornar Numero de Elementos()")
    void nrElemVector() {
        //Arrange
        int expectedResult = 4;
        int realResult;
        int [] defaultArray = {1, 2, 3, 4};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.nrElemVector();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar Maior Elemento")
    void majorElemVector() {
        //Arrange
        int expectedResult = 4;
        int realResult;
        int [] defaultArray = {1, 2, 3, 4};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.majorElemVector();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    // Main Document
    @Test
    @DisplayName("Retornar Menor Elemento - Happy Case")
    void minorElemVector() {
        //Arrange
        int expectedResult = 1;
        int realResult;
        int [] defaultArray = {1, 2, 3, 4};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.minorElemVector();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar Menor Elemento - ordem decrescente e com duplicados")
    void menorElemVectorDecrescentDuplicates() {
        //Arrange
        int expectedResult = 1;
        int realResult;
        int [] defaultArray = {4, 4, 3, 2, 2, 1};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.minorElemVector();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar Menor Elemento - ordem mista, com negativos e positivos")
    void menorElemVectorNegativesNulls() {
        //Arrange
        int expectedResult = -5;
        int realResult;
        int [] defaultArray = {-4, 3, 0, -2, 1, -5, 6};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.minorElemVector();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar Menor Elemento - vector vazio")
    void menorElemVectorEmpty() {
        //Arrange
        int expectedResult = 0;
        int realResult;
        int [] defaultArray = {};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.minorElemVector();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar Elementos Não Nulos")
    void nrElementosNNulos() {
        //Arrange
        int expectedResult = 3;
        int realResult;
        int [] defaultArray = {1, 3, 4, 0};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.nrElementosNNulos();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar Média de todos os elementos")
    void mediaElem() {
        //Arrange
        double expectedResult = 2.4;
        double realResult;
        int [] defaultArray = {1, 2, 3, 4, 2};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.mediaElem();

        //Assert
        assertEquals(expectedResult, realResult, 0.01);
    }

    @Test
    @DisplayName("Retornar Média dos Pares - com happy case")
    void mediaPares () {
        //Arrange
        double expectedResult = 2.67;
        double realResult;
        int [] defaultArray = {1, 2, 3, 4, 2, 7};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.mediaPares();

        //Assert
        assertEquals(expectedResult, realResult, 0.01);
    }

    @Test
    @DisplayName("Retornar Média dos Pares - só impares")
    void mediaParesSoImpares () {
        //Arrange
        double expectedResult = 0;
        double realResult;
        int [] defaultArray = {1, 3, 7};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.mediaPares();

        //Assert
        assertEquals(expectedResult, realResult, 0.01);
    }

    @Test
    @DisplayName("Retornar Média dos Pares - com negativos e nulos")
    void mediaParesNegativesNulls () {
        //Arrange
        double expectedResult = 1.5;
        double realResult;
        int [] defaultArray = {7, 8, -5, 3, 2, -1, 0, -4};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.mediaPares();

        //Assert
        assertEquals(expectedResult, realResult, 0.01);
    }

    @Test
    @DisplayName("Retornar Média dos Pares - com vector vazio")
    void mediaParesEmptyVector () {
        //Arrange
        double expectedResult = 0;
        double realResult;
        int [] defaultArray = {};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.mediaPares();

        //Assert
        assertEquals(expectedResult, realResult, 0.01);
    }

    @Test
    @DisplayName("Retornar Média dos Multiplos de N como argumento")
    void mediaMult() {
        //Arrange
        double expectedResult = 7.5;
        double realResult;
        int [] defaultArray = {1, 2, 5, 10, 3};
        int number = 5;

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.mediaMult(number);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar False se vector não estiver vazio")
    void vectorVazio() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int [] defaultArray = {1, 2, 5, 10, 3};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.vectorVazio();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar False se vector não tiver um só elemento")
    void vectorUmElem () {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int [] defaultArray = {1, 2, 5, 10, 3};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.vectorUmElem();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar False se o vetor não tiver apenas elementos pares")
    void vectorElemPares() {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int [] defaultArray = {1, 2, 5, 10, 3};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.vectorElemPares();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar True se o vetor tiver elementos duplicados")
    void vectorDuplicados() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int [] defaultArray = {1, 2, 2, 3, 10};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.vectorDuplicados();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar True se o vetor tiver elementos duplicados no fim do array")
    void vectorDuplicadosNoFim() {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int [] defaultArray = {1, 2, 3, 10, 10};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.vectorDuplicados();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos do vetor cujo número de algarismos é superior número médio de algarismos de todos os elementos do vetor.")
    void mediaSuperiorMediaAlgarismos() {
        //Arrange
        int [] realResult;
        int [] expectedResult = {10,12,123};
        int [] defaultArray2 = {1, 2, 3, 10, 12, 123};

        //Act
        Array number1 = new Array(defaultArray2);
        realResult = number1.mediaSuperiorMediaAlgarismos();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - happy case")
    void percentParesSuperiorMedia() {
        //Arrange
        int [] expectedResult = {2,4};
        int [] realResult;
        int [] defaultArray = {1, 2, 3, 4};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - negativos e nulos")
    void percentParesSuperiorMediaNegativosNulos() {
        //Arrange
        int [] expectedResult = {-2, -232, 0, 4445};
        int [] realResult;
        int [] defaultArray = {11, -2, 32, -232, 323, 0, 4579, -8954, 4445};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - vector vazio")
    void percentParesSuperiorMediaEmptyVector() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [] defaultArray = {};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos cuja percentagem de algarismos pares é superior à média da percentagem de " +
            "algarismos pares - percentagem de algarismos pares todos iguais à media")
    void percentParesSuperiorMediaTodosIguaisAMedia() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [] defaultArray = {0,2,4,6,8};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.percentParesSuperiorMedia();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }


    @Test
    @DisplayName("Retornar elementos compostos exclusivamente por algarismos pares.")
    void elemExclAlgPares() {
        //Arrange
        int [] expectedResult = {2,4,444};
        int [] realResult;
        int [] defaultArray = {1, 2, 3, 4, 123, 444, 5656};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.elemExclAlgPares();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar elementos que são sequências crescentes do vetor")
    void elemSeqCresc() {
        //Arrange
        int [] expectedResult = {123,4567};
        int [] realResult;
        int [] defaultArray = {2, 123, 562, 4567};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.elemSeqCresc();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    //

    @Test
    @DisplayName("Retornar as capicuas existentes no vetor - happy case")
    void capicuaVector() {
        //Arrange
        int [] expectedResult = {2, 1221, 4554, 11511, 225522};
        int [] realResult;
        int [] defaultArray = {2, 1221, 562, 4554, 12345, 11511, 225522};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.capicuaVector();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar as capicuas existentes no vetor - vetor não tem capicuas")
    void capicuaVectorSemCapicuas() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [] defaultArray = {10, 562, 123, 12345};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.capicuaVector();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar as capicuas existentes no vetor - vector com negativos e nulos")
    void capicuaVectorNegativosNulos() {
        //Arrange
        int [] expectedResult = {9, -1221, -4554, 0};
        int [] realResult;
        int [] defaultArray = {9, -1221, 562, -4554, -12345, 0};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.capicuaVector();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar as capicuas existentes no vetor - vector vazio")
    void capicuaVectorEmptyVector() {
        //Arrange
        int [] expectedResult = {};
        int [] realResult;
        int [] defaultArray = {};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.capicuaVector();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar os números compostos exclusivamente por um mesmo algarismo (e.g. 222) existentes no vetor")
    void elemMesmoAlgarismo() {
        //Arrange
        int [] expectedResult = {2,22,55};
        int [] realResult;
        int [] defaultArray = {2, 22, 562, 4554, 55};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.elemMesmoAlgarismo();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar os números que não são de Amstrong existentes no vetor.")
    void notArmstrongVector() {
        //Arrange
        int [] expectedResult = {22,123,4554};
        int [] realResult;
        int [] defaultArray = {2, 22, 123, 4554, 407};

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.notArmstrongVector();

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar os elementos que contêm uma sequência crescente de pelo menos n algarismos (e.g. n=3, 347) do vetor.")
    void elemSeqCrescAlgN() {
        //Arrange
        int [] expectedResult = {4567, 45678};
        int [] realResult;
        int [] defaultArray = {2, 23, 123, 4567, 45678};
        int number = 4;

        //Act
        Array vector1 = new Array(defaultArray);
        realResult = vector1.elemSeqCrescAlgN(number);

        //Assert
        assertArrayEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar verdadeiro se o vetor for igual a um vetor passado por parâmetro.")
    void vectorIgual () {
        //Arrange
        boolean expectedResult = true;
        boolean realResult;
        int [] myVector = {1, 2, 3, 4};
        int [] vector2 = {1, 2, 3, 4};

        //Act
        Array vector1 = new Array(myVector);
        realResult = vector1.vectorIgual(vector2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Retornar falso se o vetor for diferente a um vetor passado por parâmetro.")
    void vectorIgualVetoresDiferentes () {
        //Arrange
        boolean expectedResult = false;
        boolean realResult;
        int [] myVector = {1, 2, 3, 4};
        int [] vector2 = {};

        //Act
        Array vector1 = new Array(myVector);
        realResult = vector1.vectorIgual(vector2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyBy2 - happy case")
    void multiplyBy2 () {
        //Arrange
        int [] arrayMultiplicado = {2, 4, 6, 8};
        int [] defaultArray2 = {1, 2, 3, 4};
        int number = 2;

        //Act
        Array expectedResult = new Array(arrayMultiplicado);
        Array realResult = new Array(defaultArray2);
        realResult.multiplyByNumber(number);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyBy0")
    void multiplyBy0 () {
        //Arrange
        int [] arrayMultiplicado = {0, 0, 0, 0};
        int [] defaultArray2 = {1, 0, -3, 100};
        int number = 0;

        //Act
        Array expectedResult = new Array(arrayMultiplicado);
        Array realResult = new Array(defaultArray2);
        realResult.multiplyByNumber(number);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyBy2 - empty vector")
    void multiplyBy2EmptyVector () {
        //Arrange
        int [] arrayMultiplicado = {};
        int [] defaultArray2 = {};
        int number = 2;

        //Act
        Array expectedResult = new Array(arrayMultiplicado);
        Array realResult = new Array(defaultArray2);
        realResult.multiplyByNumber(number);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyByVector - happy case")
    void multiplyByVector () {
        //Arrange
        int [] defaultArray1 = {1, 2, 3, 4};
        int [] defaultArray2 = {2, 4, 6, 8};
        int [] arrayResultado = {2, 8, 18, 32};

        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray1);
        realResult.multiplyByVector(defaultArray2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyByVector com elementos positivos, negativos e nulos")
    void multiplyByVectorNegativeNullsPositives () {
        //Arrange
        int [] defaultArray1 = {-1, 2, 0, -4};
        int [] defaultArray2 = {-2, 0, 6, 8};
        int [] arrayResultado = {2, 0, 0, -32};

        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray1);
        realResult.multiplyByVector(defaultArray2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test multiplyByVector - vector dado vazio")
    void multiplyByEmptyVector () {
        //Arrange
        int [] defaultArray1 = {1, 2, 3, 4};
        int [] defaultArray2 = {};
        int [] arrayResultado = {1, 2, 3, 4};

        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray1);
        realResult.multiplyByVector(defaultArray2);

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test invertOrder - happy case")
    void invertOrder () {
        //Arrange
        int [] arrayResultado = {1, 2, 3, 4};
        int [] defaultArray2 = {4, 3, 2, 1};

        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray2);
        realResult.invertOrder();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test invertOrder - vector com positivos, nulos e negativos")
    void invertOrderVectorNegativeNullsPositives () {
        //Arrange
        int [] defaultArray1 = {-1, 2, 0, -4};
        int [] arrayResultado = {-4, 0, 2, -1};


        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray1);
        realResult.invertOrder();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test invertOrder - vector dado vazio")
    void invertOrderEmptyVector () {
        //Arrange
        int [] arrayResultado = {};
        int [] defaultArray2 = {};

        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray2);
        realResult.invertOrder();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    @DisplayName("Test invertOrder - vector invertido é igual ao vector normal")
    void invertOrderInvertedVectorEqualsVector () {
        //Arrange
        int [] defaultArray1 = { 7, 12, 12, 7};
        int [] arrayResultado = { 7, 12, 12, 7};


        //Act
        Array expectedResult = new Array(arrayResultado);
        Array realResult = new Array(defaultArray1);
        realResult.invertOrder();

        //Assert
        assertEquals(expectedResult, realResult);
    }

}