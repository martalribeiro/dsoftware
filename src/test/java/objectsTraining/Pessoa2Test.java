package objectsTraining;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class Pessoa2Test {

    //6. Adicione um irmão.

    @Test
    @DisplayName("Adicione um irmão - happy case")
    void adicionarIrmao () {
        //Arrange
        Pessoa2 p1 = new Pessoa2("Ana", "01-01-1990");
        Pessoa2 p2 = new Pessoa2("Joana", "02-05-1980");
        boolean expected = true;

        //Act
        p1.adicionarIrmao(p2);
        boolean result = p1.getIrmaos().contains(p2);


        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Adicionar irmao e comparar com pessoa que não é irmao")
    void adicionarIrmaoComparar () {
        //Arrange
        Pessoa2 p1 = new Pessoa2("Ana", "01-01-1990");
        Pessoa2 p2 = new Pessoa2("Joana", "02-05-1980");
        Pessoa2 p3 = new Pessoa2("Pedro", "01-01-1992");
        boolean expected = false;

        //Act
        p1.adicionarIrmao(p2);
        boolean result = p1.getIrmaos().contains(p3);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Adicionar 60 irmaos - verificar se todos os irmaos foram adicionados")
    void adicionarVáriosIrmao () {
        //Arrange
        Pessoa2 p1 = new Pessoa2("Ana", "01-01-1990");
        Pessoa2 p2 = new Pessoa2("Joana", "02-05-1980");

        //Act
        //Add siblings to Ana
        for (int i = 0; i < 61; i++) {
            p1.adicionarIrmao(p2);
        }

        HashSet<Pessoa2> irmaosAna = p1.getIrmaos();
        boolean valid = true;
        for (int i = 0; i < 61; i++) {
            if (! irmaosAna.contains(p2) ){
                valid = false;
            }
        }

        //Assert
        assertTrue(valid);
    }

    //7. Retire um irmao

    @Test
    @DisplayName("Retire um irmão - happy case")
    void retirarIrmao () {
        //Arrange
        Pessoa2 p1 = new Pessoa2("Ana", "01-01-1990");
        Pessoa2 p2 = new Pessoa2("Joana", "02-05-1980");
        Pessoa2 p3 = new Pessoa2("Maria", "06-06-2000");
        Pessoa2 p4 = new Pessoa2("Mafalda", "03-10-2005");
        boolean expected = true;

        //Act
        p1.adicionarIrmao(p2);
        p1.adicionarIrmao(p3);
        p1.adicionarIrmao(p4);
        p1.retirarIrmao(p2);
        boolean result = p1.getIrmaos().contains(p3);
        boolean result2 = p1.getIrmaos().contains(p4);

        //Assert
        assertEquals(expected, result);
        assertEquals(expected, result2);
    }

    //8. Determine se duas pessoas têm os mesmos irmãos.

    @Test
    @DisplayName("mesmosIrmaos - Ambas as pessoas têm os mesmo irmãos na mesma ordem")
    void mesmosIrmaosMesmaOrdem () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(p2);
        p2.adicionarIrmao(p1);
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);
        p2.adicionarIrmao(s2);

        //Assert
        assertTrue(p1.mesmosIrmaos(p2));
    }

    @Test
    @DisplayName("mesmosIrmaos - Ambas as pessoas têm os mesmo irmãos mas em ordens diferentes")
    void mesmosIrmaosOrdemDiferente () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null );
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null );

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null );
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null );

        // Add siblings
        p1.adicionarIrmao(p2);
        p2.adicionarIrmao(p1);
        p1.adicionarIrmao(s1);
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        assertTrue(p1.mesmosIrmaos(p2));
    }


    @Test
    @DisplayName("mesmosIrmaos - array Irmaos com número de irmaos diferentes")
    void mesmoIrmaosQuantidadeDiferenteIrmaos () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null);

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null);
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null);

        // Add siblings
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        assertFalse(p1.mesmosIrmaos(p2));
    }

    @Test
    @DisplayName("mesmosIrmaos - irmaos diferentes")
    void mesmoIrmaosIrmaosDiferentes () {
        // Persons
        Pessoa p1 = new Pessoa("Ana", "01-01-1990", null, null);
        Pessoa p2 = new Pessoa("Joana", "02-05-1980", null, null);

        // Sibilings
        Pessoa s1 = new Pessoa("Marta", "02-04-1980", null, null);
        Pessoa s2 = new Pessoa("Pedro", "02-03-1980", null, null);

        // Add siblings
        p1.adicionarIrmao(s2);
        p2.adicionarIrmao(s1);

        //Assert
        assertFalse(p1.mesmosIrmaos(p2));
    }

}