package pt.ipp.isep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios2Test {

    @Test
    void pares() {
        Exercícios2 c = new Exercícios2();
        boolean result = c.pares(5);
        assertEquals(false, result);
    }
    @Test
    void pares2() {
        Exercícios2 c = new Exercícios2();
        boolean result = c.pares(0);
        assertEquals(true, result);
    }
    @Test
    void somaPares() {
        Exercícios2 c = new Exercícios2();
        long result = c.somaPares(0,10);
        assertEquals(30, result);
    }
    @Test
    void somaPares2() {
        Exercícios2 c = new Exercícios2();
        long result = c.somaPares(0,10);
        assertEquals(30, result);
    }
}