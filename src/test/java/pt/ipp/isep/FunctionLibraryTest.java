package pt.ipp.isep;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FunctionLibraryTest {

    @Test
    @DisplayName("Sequência decrescentes em sentido lato - Happy path")
    void elemSequenciasDecrescentesSLTest() {
        int [] vec = new int[]{1, 2, 3, 1, 1, 4, 2};

        int [] expectedResult = new int[]{1,2};
        int[] result = FunctionLibrary.elemSequenciasDecrescentesSL(vec);
        assertArrayEquals(result, expectedResult);
    }

    @Test
    @DisplayName("Sequência decrescentes em sentido lato - sequencia decrescente no início")
    void elemSequenciasDecrescentesSLFinal() {
        int [] vec = new int[]{5, 0, 1, 2, 3, 1, 1, 4, 2};

        int [] expectedResult = new int[]{1,2};
        int[] result = FunctionLibrary.elemSequenciasDecrescentesSL(vec);
        assertArrayEquals(result, expectedResult);
    }

    @Test
    @DisplayName("Sequência Crescentes em sentido lato - Vetor de dimensão 1")
    void elemSequenciasDecrescentesSLTestInvalidLenght() {
        int [] vec = new int[]{3};

        int [] expectedResult = new int[0];
        int[] result = FunctionLibrary.elemSequenciasDecrescentesSL(vec);
        assertArrayEquals(result, expectedResult);
    }

    @Test
    @DisplayName("Sequência Crescentes em sentido lato - sem sequência")
    void elemSequenciasDecrescentesSLTestNoSequence() {
        int [] vec = new int[]{1, 2, 3, 4, 18, 20};

        int [] expectedResult = new int[]{1, 2, 3, 4, 18, 20};
        int[] result = FunctionLibrary.elemSequenciasDecrescentesSL(vec);
        assertArrayEquals(result, expectedResult);
    }
}