package pt.ipp.isep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios5Test {

    @Test
    void retornarMultiplo3() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo3(1,10);
        assertArrayEquals(new int[]{3,6,9}, result);
    }
    @Test
    void retornarMultiplo3trocarl1l2() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo3(10,1);
        assertArrayEquals(new int[]{3,6,9}, result);
    }
    @Test
    void retornarMultiplo30() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo3(0,10);
        assertArrayEquals(new int[]{0,3,6,9}, result);
    }
    @Test
    void retornarMultiplo3nada() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo3(10,11);
        assertArrayEquals(new int[]{}, result);
    }
    @Test
    void retornarMultiploDesc() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiploDesc(2,1,7);
        assertArrayEquals(new int[]{2,4,6}, result);
    }
    @Test
    void retornarMultiploDesc2() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiploDesc(3,1,7);
        assertArrayEquals(new int[]{3,6}, result);
    }
    @Test
    void retornarMultiploDesc3() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiploDesc(-2,-7,-1);
        assertArrayEquals(new int[]{-6,-4,-2}, result);
    }
    @Test
    void retornarMultiploDesc4() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiploDesc(5,6,9);
        assertArrayEquals(new int[]{}, result);
    }
    @Test
    void retornarMultiploDesc5() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiploDesc(0,6,9);
        assertArrayEquals(new int[]{-1}, result);
    }
    @Test
    void retornarMultiplo351() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo35(1,30);
        assertArrayEquals(new int[]{15,30}, result);
    }
    @Test
    void retornarMultiplo352() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo35(30,1);
        assertArrayEquals(new int[]{15,30}, result);
    }
    @Test
    void retornarMultiplo353() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo35(1,4);
        assertArrayEquals(new int[]{}, result);
    }
    @Test
    void retornarMultiplo354() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplo35(-30,-1);
        assertArrayEquals(new int[]{-30,-15}, result);
    }
    @Test
    void retornarMultiplo2Desc() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplos2Desc(2,4, 1,9);
        assertArrayEquals(new int[]{4,8}, result);
    }
    @Test
    void retornarMultiplo2Desc2() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplos2Desc(-2,-4, -1,-9);
        assertArrayEquals(new int[]{-8,-4}, result);
    }
    @Test
    void retornarMultiplo2Desc3() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplos2Desc(-2,-4, -9,-1);
        assertArrayEquals(new int[]{-8,-4}, result);
    }
    @Test
    void retornarMultiplo2Desc4() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplos2Desc(5, 10, 1,9);
        assertArrayEquals(new int[]{}, result);
    }
    @Test
    void retornarMultiplo2Desc5() {
        Exercícios5 c = new Exercícios5();
        int[] result = c.retornarMultiplos2Desc(0, 10, 1,9);
        assertArrayEquals(new int[]{-1}, result);
    }
}