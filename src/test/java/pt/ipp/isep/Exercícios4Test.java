package pt.ipp.isep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios4Test {

    @Test
    void medAlgarismoLongoPos() {
        Exercícios4 c = new Exercícios4();
        float result = c.medAlgarismoLongo(12345);
        assertEquals(3, result);
    }
    @Test
    void medAlgarismoLongoNeg() {
        Exercícios4 c = new Exercícios4();
        float result = c.medAlgarismoLongo(-25896);
        assertEquals(6, result);
    }
    @Test
    void medAlgarismoLongo0() {
        Exercícios4 c = new Exercícios4();
        float result = c.medAlgarismoLongo(0);
        assertEquals(-1, result);
    }
    @Test
    void medAlgarismoLongoReal() {
        Exercícios4 c = new Exercícios4();
        float result = c.medAlgarismoLongo(8842);
        assertEquals(5.5, result);
    }
    @Test
    void somaAlgarismoLongoPositivo() {
        Exercícios4 c = new Exercícios4();
        float result = c.somaAlgarismoLongo(8842);
        assertEquals(22, result);
    }

    @Test
    void somaAlgarismoLongoNegativo() {
        Exercícios4 c = new Exercícios4();
        float result = c.somaAlgarismoLongo(-8842);
        assertEquals(22, result);
    }
    @Test
    void somaAlgarismoLongo0() {
        Exercícios4 c = new Exercícios4();
        float result = c.somaAlgarismoLongo(0);
        assertEquals(-1, result);
    }

    @Test
    void simNumeroLongoPositivo() {
        Exercícios4 c = new Exercícios4();
        long result = c.simNumeroLongo(2456);
        assertEquals(6542, result);
    }
    @Test
    void simNumeroLongoNegativo() {
        Exercícios4 c = new Exercícios4();
        long result = c.simNumeroLongo(-2456);
        assertEquals(6542, result);
    }
    @Test
    void simNumeroLongo0() {
        Exercícios4 c = new Exercícios4();
        long result = c.simNumeroLongo(0);
        assertEquals(0, result);
    }
    @Test
    void simNumeroLongo4() {
        Exercícios4 c = new Exercícios4();
        long result = c.simNumeroLongo(4);
        assertEquals(4, result);
    }
    @Test
    void isCapicuaNaoCapicuaPos() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isCapicua(1234);
        assertEquals(false, result);
    }
    @Test
    void isCapicuaNaoCapicuaNeg() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isCapicua(-1234);
        assertEquals(false, result);
    }
    @Test
    void isCapicuaPos() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isCapicua(1221);
        assertEquals(true, result);
    }
    @Test
    void isCapicuaNeg() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isCapicua(-1221);
        assertEquals(true, result);
    }
    @Test
    void isCapicua0() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isCapicua(0);
        assertEquals(true, result);
    }
    @Test
    void isArmstrong() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isArmstrong(153);
        assertEquals(true, result);
    }
    @Test
    void isArmstrong153() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isArmstrong(153);
        assertEquals(true, result);
    }
    @Test
    void isArmstrongmenos307() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isArmstrong(-370);
        assertEquals(true, result);
    }
    @Test
    void isArmstrong444() {
        Exercícios4 c = new Exercícios4();
        boolean result = c.isArmstrong(444);
        assertEquals(false, result);
    }
    @Test
    void firstCapicua1() {
        Exercícios4 c = new Exercícios4();
        int result = c.firstCapicua(50,100);
        assertEquals(55, result);
    }
    @Test
    void firstCapicua2() {
        Exercícios4 c = new Exercícios4();
        int result = c.firstCapicua(-50,100);
        assertEquals(-44, result);
    }
    @Test
    void firstCapicua3() {
        Exercícios4 c = new Exercícios4();
        int result = c.firstCapicua(100,50);
        assertEquals(99, result);
    }
    @Test
    void greaterCapicua1() {
        Exercícios4 c = new Exercícios4();
        int result = c.greaterCapicua(100,50);
        assertEquals(99, result);
    }
    @Test
    void greaterCapicua2() {
        Exercícios4 c = new Exercícios4();
        int result = c.greaterCapicua(50,100);
        assertEquals(99, result);
    }
    @Test
    void greaterCapicua3() {
        Exercícios4 c = new Exercícios4();
        int result = c.greaterCapicua(-50,100);
        assertEquals(99, result);
    }
    @Test
    void contaCapicua1() {
        Exercícios4 c = new Exercícios4();
        int result = c.contaCapicua(50,100);
        assertEquals(5, result);
    }
    @Test
    void contaCapicua2() {
        Exercícios4 c = new Exercícios4();
        int result = c.contaCapicua(100,50);
        assertEquals(5, result);
    }
    @Test
    void contaCapicua3() {
        Exercícios4 c = new Exercícios4();
        int result = c.contaCapicua(-10,10);
        assertEquals(19, result);
    }
    @Test
    void contaCapicua4() {
        Exercícios4 c = new Exercícios4();
        int result = c.contaCapicua(10, -10);
        assertEquals(19, result);
    }

    @Test
    void firstArmstrong1 () {
        Exercícios4 c = new Exercícios4();
        int result = c.firstArmstrong(100, 410);
        assertEquals(153, result);
    }

    @Test
    void firstArmstrong2 () {
        Exercícios4 c = new Exercícios4();
        int result = c.firstArmstrong(410, 100);
        assertEquals(407, result);
    }

    @Test
    void contaArmstrong1 () {
        Exercícios4 c = new Exercícios4();
        int result = c.contaArmstrong(10, 410);
        assertEquals(4, result);
    }
    @Test
    void contaArmstrong2 () {
        Exercícios4 c = new Exercícios4();
        int result = c.contaArmstrong(0, 410);
        assertEquals(14, result);
    }
}