package pt.ipp.isep;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios9Test {

    //IX.a

    @Test
    void SequenciaCrescentesElementosInt1 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesElementosInt(new int[]{1,2,3,0,13,10,5,4,3,2,1,5,10,15,17});
        assertArrayEquals(new int[]{1,2,3,0,13,1,5,10,15,17}, result);
    }

    @Test
    void SequenciaCrescentesElementosInt2 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesElementosInt(new int[]{1,2,3,1,1,1,0,-1,-2,-3,13,10,5,4,3,2,1,5,10,15,17});
        assertArrayEquals(new int[]{1,2,3,-3,13,1,5,10,15,17}, result);
    }

    @Test
    void SequenciaCrescentesElementosInt3 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesElementosInt(new int[]{1,2,-3,-1,5,10,0,15,17,-5,-10,-20,-30,-40});
        assertArrayEquals(new int[]{1,2,-3,-1,5,10,0,15,17}, result);
    }

    @Test
    void SequenciaCrescentesElementosInt4 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesElementosInt(new int[]{});
        assertArrayEquals(new int[]{}, result);
    }

    //IX.b

    @Test
    void sequenciaCrescentesIntNrElementosGeral () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesIntNrElementos(new int[]{1,2,-3,-1,-5,15,17,-5,-10,-20,10},2);
        assertArrayEquals(new int[]{1,2,-3,-1,-5,15,17,-20,10}, result);
    }

    @Test
    void sequenciaCrescentesIntNrElementosNegativo () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesIntNrElementos(new int[]{1,2,-3,-1,-5,15,17,-5,-10,-20,10},-1);
        assertArrayEquals(new int[]{1,2,-3,-1,-5,15,17,-5,-10,-20,10}, result);
    }

    @Test
    void sequenciaCrescentesIntNrElementosGeral3 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesIntNrElementos(new int[]{1,2,3,-3,-1,-5,15,17,-5,-10,-20,10},3);
        assertArrayEquals(new int[]{1,2,3,-5,15,17}, result);
    }

    @Test
    void sequenciaCrescentesIntNrElementosGeral4 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesIntNrElementos(new int[]{1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20,10},3);
        assertArrayEquals(new int[]{1,2,3,4,20,-5,15,17}, result);
    }

    @Test
    void sequenciaCrescentesIntNrElementosGeral5 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesIntNrElementos(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20,10},3);
        assertArrayEquals(new int[]{1,2,3,4,20,-5,15,17}, result);
    }

    @Test
    void sequenciaCrescentesElementosIntNrElementos () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.sequenciaCrescentesIntNrElementos(new int[]{},2);
        assertArrayEquals(new int[]{}, result);
    }

    //IX.c
    @Test
    void maiorSequenciaCrescenteInt1 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{1,2,3,1,5,10,15,17,13,10,5});
        assertArrayEquals(new int[]{1,5,10,15,17}, result);
    }

    @Test
    void maiorSequenciaCrescenteInt2 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{1,2,3,1,5,10,0,15,17,13,10,5});
        assertArrayEquals(new int[]{1,2,3}, result);
    }

    @Test
    void maiorSequenciaCrescenteInt3 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{1,2,3,1,5,10,0,15,17,5,10,20,30,40});
        assertArrayEquals(new int[]{5,10,20,30,40}, result);
    }

    @Test
    void maiorSequenciaCrescenteInt4 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{1,2,3,1,5,10,0,15,17,5,10,20,30,40});
        assertArrayEquals(new int[]{5,10,20,30,40}, result);
    }

    @Test
    void maiorSequenciaCrescenteInt5 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{1,2,-3,-1,5,10,0,15,17,-5,-10,-20,-30,-40});
        assertArrayEquals(new int[]{-3,-1,5,10}, result);
    }

    @Test
    void maiorSequenciaCrescenteInt6 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{1,0,-1,1,5,10,15,17,13,10,5});
        assertArrayEquals(new int[]{-1,1,5,10,15,17}, result);
    }

    @Test
    void maiorSequenciaCrescenteInt7 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.maiorSequenciaDeElementosInt(new int[]{});
        assertArrayEquals(new int[]{}, result);
    }

    //IX.d

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor")
    void maiorSequenciaDeElementosGeralTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.23f,2.34f,2.35f,3.56f},0.01f);
        assertArrayEquals(new float []{1.23f,2.34f},result);
    }

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor com elementos positivos e negativos")
    void maiorSequenciaDeElementosNegativosTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.23f,2.34f,2.37f,-3.56f},0.01f);
        assertArrayEquals(new float []{1.23f,2.34f,2.37f},result);
    }

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor dado um valor delta diferente")
    void maiorSequenciaDeElementosNovoDeltaTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.23f,2.34f,2.35f,3.56f},0.1f);
        assertArrayEquals(new float []{1.23f,2.34f},result);
    }

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor com elementos nulos")
    void maiorSequenciaDeElementosNulosTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.23f,0f,2.34f,2.35f,-3.56f},0.01f);
        assertArrayEquals(new float []{0f,2.34f},result);
    }

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor vazio")
    void maiorSequenciaDeElementosVazioTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{},0.01f);
        assertArrayEquals(new float []{},result);
    }

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor dado um delta com mais casas decimais")
    void maiorSequenciaDeElementosMaisCasasDecimaisTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.2345f,2.3456f,2.3459f,2.3517f},0.001f);
        assertArrayEquals(new float []{1.2345f,2.3456f},result);
    }

    @Test
    @DisplayName("Retornar a maior de sequência de elementos de um vetor com elementos negativos")
    void maiorSequenciaDeElementosSequenciaNegativosTest() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{-3.56f,-2.35f,-2.34f,-1.23f},0.01f);
        assertArrayEquals(new float []{-3.56f,-2.35f},result);
    }

    ////TESTES SUPLEMENTARES IX.d

    @Test
    void maiorSequenciaCrescenteGeral() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f},0.1f);
        assertArrayEquals(new float[]{1.1f,5.5f,10.3f,15.2f,17.2f}, result);
    }

    @Test
    void maiorSequenciaCrescenteAlterarDelta () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.01f,2.02f,3.03f,1.01f,5.05f,10.03f,15.02f,17.02f,13.05f,10.02f,5.01f},0.01f);
        assertArrayEquals(new float[]{1.01f,5.05f,10.03f,15.02f,17.02f}, result);
    }

    @Test
    void maiorSequenciaCrescenteAumentandoUmaCasaDecimal () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.01f,2.02f,3.03f,1.01f,5.05f,10.03f,15.02f,17.02f,13.05f,10.02f,5.01f},0.01f);
        assertArrayEquals(new float[]{1.01f,5.05f,10.03f,15.02f,17.02f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral1 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f},0.1f);
        assertArrayEquals(new float[]{1.1f,5.5f,10.3f,15.2f,17.2f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral3 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.01485f,2.02485f,3.03485f,1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f},0.01f);
        assertArrayEquals(new float[]{1.01485f,2.02485f,3.03485f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral4 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.01485f,2.02485f,3.03485f,1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f},0.00001f);
        assertArrayEquals(new float[]{1.01485f,2.02485f,3.03485f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral5 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,30.2f,20.5f,25.2f,30.5f,40.1f,50.6f},0.1f);
        assertArrayEquals(new float[]{1.1f,5.5f,10.3f,15.2f,17.2f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral6 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{-1.1f,-2.2f,-3.3f,-1.1f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f},0.1f);
        assertArrayEquals(new float[]{-3.3f,-1.1f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral7 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.1f,2.2f,3.3f,-1.1f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f},0.1f);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral8 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{1.1f,2.2f,3.3f,4.4f,1.1f,5.5f,10.3f,0,15.2f,17.2f,13.5f,10.2f},0.1f);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,4.4f}, result);
    }

    @Test
    void maiorSequenciaElementosGeral9 () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.maiorSequenciaDeElementos(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f},0.1f);
        assertArrayEquals(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f}, result);
    }

    @Test
    void maiorSequenciaDeElementosSequenciaNaoTemSequenciaTest1 () {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.23f,2.34f,2.37f,3.56f},0.01f);
        assertArrayEquals(new float []{1.23f,2.34f,2.37f,3.56f},result);
    }

    @Test
    void maiorSequenciaDeElementosNovoDeltaTest2() {
        Exercícios9 c = new Exercícios9();
        float[] result = c.maiorSequenciaDeElementos(new float[]{1.23f, 2.34f, -2.37f, 1.23f, 2.34f, 2.37f, 3.56f}, 0.01f);
        assertArrayEquals(new float[]{-2.37f,1.23f, 2.34f, 2.37f, 3.56f}, result);
    }

    @Test
    void maiorSequenciaDeElementosNovoDeltaTest3() {
        Exercícios9 c= new Exercícios9();
        float [] result= c.maiorSequenciaDeElementos(new float []{1.23f,-2.34f,2.35f,3.56f},0.1f);
        assertArrayEquals(new float []{-2.34f,2.35f,3.56f},result);
    }

    //IX.e

    @Test
    void semSequenciasDecrescentes () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{1,2,0,13,10,5,4,3,2,1,5,10,15,17});
        assertArrayEquals(new int[]{1,5,10,15,17}, result);
    }

    @Test
    void semSequenciasDecrescentes2 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{1,2,2,0,13,10,5,4,4,3,2,2,1,5,10,15,17});
        assertArrayEquals(new int[]{1,5,10,15,17}, result);
    }

    @Test
    void semSequenciasDecrescentes3 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{1,2,3,1,1,1,0,5,6,7,-1,-2,-3,5,10,15,17});
        assertArrayEquals(new int[]{1,2,5,6,5,10,15,17}, result);
    }

    @Test
    void semSequenciasDecrescentes4 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{1,2,-3,-1,5,10,0,15,17,-5,-10,-20,-30,-40});
        assertArrayEquals(new int[]{1,-1,5,15}, result);
    }

    @Test
    void semSequenciasDecrescentes5 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{10,1,2,-3,-1,5,10,0,15,17,-5,-10,-20,-30,-40});
        assertArrayEquals(new int[]{-1,5,15}, result);
    }

    @Test
    void semSequenciasDecrescentes6 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{});
        assertArrayEquals(new int[]{}, result);
    }

    @Test
    void semSequenciasDecrescentes7 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentes(new int[]{});
        assertArrayEquals(new int[]{}, result);
    }

    //IX.f

    @Test
    void semSequenciasDecrescentesNrElem () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,3,1,2,3}, 2);
        assertArrayEquals(new int[]{1,2,2,3}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem1 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,3,1,2,3}, 3);
        assertArrayEquals(new int[]{1,2,3,1,2,3}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem2 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,3,1,1,1,0,5,6,7,-1,-2,-3,5,10,15,17}, 2);
        assertArrayEquals(new int[]{1,2,5,6,5,10,15,17}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem3 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,-3,-4,-1,5,10,2,15,17,-5,-10,-20,5,6}, 3);
        assertArrayEquals(new int[]{1,-1,5,10,2,15,5,6}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem4 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,0,13,10,5,4,3,2,1,5,10,15,17}, -5);
        assertArrayEquals(new int[]{1,2,0,13,10,5,4,3,2,1,5,10,15,17}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem5 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,3,-3,-1,-5,15,17,-5,-10,10},3);
        assertArrayEquals(new int[]{1,2,3,-3,-1,-5,15,10}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem7 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20,10},3);
        assertArrayEquals(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,10}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem8 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20,10},3);
        assertArrayEquals(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,10}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem9 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20},5);
        assertArrayEquals(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem10 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20,-21},3);
        assertArrayEquals(new int[]{5,1,2,3,4,20,-3,-1,-5,15,17,-5}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem11 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{5,1,0,2,3,4,20,-3,-1,-5,15,17,-5,0,-10,-20,10},3);
        assertArrayEquals(new int[]{2,3,4,20,-3,-1,-5,15,17,-5,10}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem12 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{},2);
        assertArrayEquals(new int[]{}, result);
    }

    @Test
    void semSequenciasDecrescentesNrElem13 () {
        Exercícios9 c = new Exercícios9();
        int [] result = c.semSequenciasDecrescentesNrElem(new int[]{1,2,3,4,5,6,7,8,9},2);
        assertArrayEquals(new int[]{1,2,3,4,5,6,7,8,9}, result);
    }

    //IX.g

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente com resolução de comparação delta = 0,1 - teste básico")
    void semMaiorSequenciaElementosGeral() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f},0.1f);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,13.5f,10.2f,5.1f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente alterando a resolução de comparação delta para 0,01")
    void semMaiorSequenciaCrescenteAlterarDelta() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.01f,2.02f,3.03f,1.01f,5.05f,10.03f,15.02f,17.02f,13.05f,10.02f,5.01f},0.01f);
        assertArrayEquals(new float[]{1.01f,2.02f,3.03f,13.05f,10.02f,5.01f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente alterando a resolução de comparação delta para 0,05")
    void semMaiorSequenciaCrescenteAlterarParaUmDeltaMaior() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.01f,2.02f,3.03f,1.01f,5.05f,5.03f,10.03f,10.01f,15.02f,17.02f,13.05f},0.05f);
        assertArrayEquals(new float[]{1.01f,2.02f,3.03f,13.05f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente aumentando as casas decimais do vector dado")
    void semMaiorSequenciaElementosAumentandoCasasDecimais () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.01485f,2.02485f,3.03485f,1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f},0.01f);
        assertArrayEquals(new float[]{1.01485f,2.02485f,3.03485f,13.05485f,10.02485f,5.01485f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente aumentado a resolução de comparação delta para 0.00001")
    void semMaiorSequenciaElementosAlterarDeltaParaCincoCasasDecimais () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.01485f,2.02485f,3.03485f,1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f},0.00001f);
        assertArrayEquals(new float[]{1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente com duas sequências crescentes com número igual de elementos")
    void semMaiorSequenciaElementosComDuasSequenciasCrescentesComNumeroIgualDeElementos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,30.2f,20.5f,25.2f,30.5f,40.1f,50.6f},0.1f);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,13.5f,10.2f,30.2f,20.5f,25.2f,30.5f,40.1f,50.6f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente com sequencia crescente no final do vector")
    void semMaiorSequenciaElementosComDuasSequenciasCrescentesComSequenciaCrescenteFinalVetor () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.1f,2.2f,3.3f,-13.5f,100.2f,30.2f,30.5f,40.1f,50.6f,1.1f,5.5f,10.3f,15.2f,17.2f},0.1f);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,-13.5f,100.2f,30.2f,30.5f,40.1f,50.6f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente com todos os elementos do vector negativos")
    void semMaiorSequenciaElementosVectorTodosNegativos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{-1.1f,-2.2f,-3.3f,-1.1f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f},0.1f);
        assertArrayEquals(new float[]{-1.1f,-2.2f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente composta por números negativos")
    void semMaiorSequenciaElementosNegativos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.1f,2.2f,3.3f,-1.1f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f},0.1f);
        assertArrayEquals(new float[]{-1.1f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente com elementos nulos no vector")
    void semMaiorSequenciaElementosComElementosNulos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{1.1f,2.2f,3.3f,4.4f,1.1f,5.5f,10.3f,0,15.2f,17.2f,13.5f,10.2f},0.1f);
        assertArrayEquals(new float[]{1.1f,5.5f,10.3f,0,15.2f,17.2f,13.5f,10.2f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem maior sequência crescente composta por numeros positivos e negativos")
    void semMaiorSequenciaElementosNegativosPositivos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f},0.1f);
        assertArrayEquals(new float[]{1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f}, result);
    }

    @Test
    void semMaiorSequenciaCrescenteVazio () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{}, 0.1f);
        assertArrayEquals(new float[]{}, result);
    }

    @Test
    void semMaiorSequenciaCrescenteSemSequenciaCrescente () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMaiorSequenciasCrescente(new float[]{5.5f, 2.3f, 0,-2.1f, -5.5f},0.1f);
        assertArrayEquals(new float[]{}, result);
    }

    //IX.h

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente com resolução de comparação delta = 0,1 - teste básico")
    void semMenorSequenciaElementosGeral() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f},0.1f, 2);
        assertArrayEquals(new float[]{1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente com resolução de comparação delta = 0,1 - teste básico")
    void semMenorSequenciaElementosNElemNegativo() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f},0.1f, -5);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,5.1f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente alterando a resolução de comparação delta para 0,01")
    void semMenorSequenciaCrescenteAlterarDelta() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.01f,2.02f,1.01f,5.05f,10.03f,15.02f,17.02f,13.05f,10.02f,5.01f},0.01f, 3);
        assertArrayEquals(new float[]{1.01f,2.02f,13.05f,10.02f,5.01f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente alterando a resolução de comparação delta para 0,05")
    void semMenorSequenciaCrescenteAlterarParaUmDeltaMaiorEAlterarValoresSequencia() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.01f,2.02f,1.01f,5.25f,5.03f,10.23f,10.05f,15.02f,17.02f,13.05f},0.05f, 3);
        assertArrayEquals(new float[]{1.01f,2.02f,1.01f,5.25f,5.03f,10.23f,13.05f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente transformam sequências decrescentes em crescentes em sentido lato")
    void semMenorSequenciaCrescenteAlterandoSequenciasDecrescentesParaCrescentes() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.01f,2.02f,1.01f,5.03f,5.05f,10.05f,10.02f,9.02f,17.02f,13.05f},0.05f, 3);
        assertArrayEquals(new float[]{1.01f,2.02f,9.02f,17.02f,13.05f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente aumentado a resolução de comparação delta para 0.00001")
    void semMenorSequenciaElementosAlterarDeltaParaCincoCasasDecimais () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.01485f,2.02485f,3.03485f,1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f},0.00001f, 2);
        assertArrayEquals(new float[]{1.01485f,1.01256f,5.05485f,10.03485f,10.03125f,15.02485f,17.02485f,13.05485f,10.02485f,5.01485f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente com duas sequências crescentes com número igual de elementos")
    void semMenorSequenciaElementosComDuasSequenciasCrescentesComNumeroIgualDeElementos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,10.2f,30.2f,20.5f,25.2f,30.5f,40.1f,50.6f},0.1f, 4);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,13.5f,10.2f,30.2f,20.5f,25.2f,30.5f,40.1f,50.6f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente com sequencia crescente no final do vector")
    void semMenorSequenciaElementosComDuasSequenciasCrescentesComSequenciaCrescenteFinalVetor () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{1.1f,2.2f,3.3f,-13.5f,100.2f,30.2f,30.5f,40.1f,50.6f,1.1f,5.5f,10.3f,15.2f,17.2f},0.1f, 5);
        assertArrayEquals(new float[]{1.1f,2.2f,3.3f,-13.5f,100.2f,30.2f,30.5f,40.1f,50.6f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente com todos os elementos do vector negativos")
    void semMenorSequenciaElementosVectorTodosNegativos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{-1.1f,-2.2f,-3.3f,-1.1f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f},0.1f, 2);
        assertArrayEquals(new float[]{-1.1f,-2.2f,-5.5f,-10.3f,-15.2f,-17.2f,-13.5f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente composta por numeros positivos e negativos")
    void semMenorSequenciaElementosNegativosPositivosENulos () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,16.2f},0.1f, 2);
        assertArrayEquals(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f}, result);
    }

    @Test
    @DisplayName("Retornar vector sem menor sequência crescente composta por numeros positivos e negativos")
    void semMenorSequenciaElementosNaoHaSequencias() {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,16.2f},0.1f, 7);
        assertArrayEquals(new float[]{-15.3f,-8.65f,0,1.1f,2.2f,3.3f,1.1f,5.5f,10.3f,15.2f,17.2f,13.5f,16.2f}, result);
    }

    @Test
    void semMenorSequenciaCrescenteVazio () {
        Exercícios9 c = new Exercícios9();
        float [] result = c.semMenorSequenciasCrescenteNElem(new float[]{},0.1f,2);
        assertArrayEquals(new float[]{}, result);
    }
}