package pt.ipp.isep;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios6Test {

    @Test
    void retornarElementosVector() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarElementosVector(new int[]{2,4,6,8,10});
        assertEquals(5, result);
    }
    @Test
    void retornarElementosVector2() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarElementosVector(new int[]{-2,-4,-6,-8,-10});
        assertEquals(5, result);
    }
    @Test
    void retornarElementosVector3() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarElementosVector(new int[]{0,-2,-4,-6,-8,-10});
        assertEquals(6, result);
    }
    @Test
    void retornarMaximoVector1() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMaximoVector(new int[]{0,1,2,3,4,5});
        assertEquals(5, result);
    }
    @Test
    void retornarMaximoVector2() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMaximoVector(new int[]{5,4,3});
        assertEquals(5, result);
    }
    @Test
    void retornarMaximoVector3() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMaximoVector(new int[]{1,10,2,11,3,12,4,13,5,14,6,15});
        assertEquals(15, result);
    }
    @Test
    void retornarMaximoVector4() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMaximoVector(new int[]{-5,4,-6,5});
        assertEquals(5, result);
    }
    @Test
    void retornarMinimoVector1() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMinimoVector(new int[]{3,4,5});
        assertEquals(3,result);
    }
    @Test
    void retornarMinimoVector2() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMinimoVector(new int[]{5,4,3});
        assertEquals(3,result);
    }
    @Test
    void retornarMinimoVector3() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarMinimoVector(new int[]{-5,4,-6,5});
        assertEquals(-6, result);
    }
    @Test
    void retornarElemNNulos1() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarElemNNulos(new int[]{1,0,2,-2,5,-9,0,4,-6,0});
        assertEquals(7, result);
    }
    @Test
    void retornarElemNNulos2() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarElemNNulos(new int[]{0,0,0,0,0,0,0,0,0,0});
        assertEquals(0, result);
    }
    @Test
    void retornarElemNNulos3() {
        Exercícios6 c = new Exercícios6();
        int result = c.retornarElemNNulos(new int[]{5,3,4});
        assertEquals(3, result);
    }
    @Test
    void medElements() {
        Exercícios6 c = new Exercícios6();
        double result = c.medElements(new int[]{2,2});
        assertEquals(2, result);
    }
    @Test
    void medElements2() {
        Exercícios6 c = new Exercícios6();
        double result = c.medElements(new int[]{2,2,-2,-3});
        assertEquals(-0.25, result);
    }
    @Test
    void medElements3() {
        Exercícios6 c = new Exercícios6();
        double result = c.medElements(new int[]{2,0,2,3,2,1,1});
        assertEquals(1.57, result);
    }
    @Test
    void medPares1() {
        Exercícios6 c = new Exercícios6();
        double result = c.medPares(new int[]{2,5,8,6,3,2,2});
        assertEquals(4, result);
    }
    @Test
    void medPares2() {
        Exercícios6 c = new Exercícios6();
        double result = c.medPares(new int[]{2,5,-8,6,3,2,2});
        assertEquals(0.8, result);
    }
    @Test
    void medPares3() {
        Exercícios6 c = new Exercícios6();
        double result = c.medPares(new int[]{5,8,6,0,3,-2});
        assertEquals(3, result);
    }
    @Test
    void medMultArg() {
        Exercícios6 c = new Exercícios6();
        double result = c.medMultArg(new int[]{3,9,5,12,18,4,7},3);
        assertEquals(10.5,result);
    }
    @Test
    void medMultArg2() {
        Exercícios6 c = new Exercícios6();
        double result = c.medMultArg(new int[]{3,9,5,12,15,4,7},5);
        assertEquals(10,result);
    }
    @Test
    void medMultArg3() {
        Exercícios6 c = new Exercícios6();
        double result = c.medMultArg(new int[]{-3,9,5,-12,15,0,4,7},5);
        assertEquals(6.67,result);
    }
    @Test
    void medMultArg4() {
        Exercícios6 c = new Exercícios6();
        double result = c.medMultArg(new int[]{-3,9,0,-12,15,0,4,7},0);
        assertEquals(-1,result);
    }
    @Test
    void vectVazio1() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectVazio(new int[]{});
        assertEquals(true,result);
    }
    @Test
    void vectVazio2() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectVazio(new int[]{0});
        assertEquals(false,result);
    }
    @Test
    void vectVazio3() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectVazio(new int[]{0,-1,0,3});
        assertEquals(false,result);
    }
    @Test
    void vectUm1() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectUm(new int[]{0});
        assertEquals(true,result);
    }
    @Test
    void vectUm2() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectUm(new int[]{0,-1,1});
        assertEquals(false,result);
    }
    @Test
    void vectUm3() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectUm(new int[]{3,2,1});
        assertEquals(false,result);
    }
    @Test
    void vectPares() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectPares(new int[]{2,4,6});
        assertEquals(true, result);
    }
    @Test
    void vectPares2() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectPares(new int[]{2,-3,4,6,5});
        assertEquals(false, result);
    }
    @Test
    void vectPares3() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectPares(new int[]{-3,4,6,5});
        assertEquals(false, result);
    }
    @Test
    void vectPares4() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectPares(new int[]{-2,4,6,0});
        assertEquals(true, result);
    }
    @Test
    void vectDupl1() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectDupl(new int[]{1,2,3,4,4,5,6});
        assertEquals(true, result);
    }
    @Test
    void vectDupl2() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectDupl(new int[]{1,-2,3,4,-5,6});
        assertEquals(false, result);
    }
    @Test
    void vectDupl3() {
        Exercícios6 c = new Exercícios6();
        boolean result = c.vectDupl(new int[]{1,0,-3,-4,5,5,-6});
        assertEquals(true, result);
    }
}