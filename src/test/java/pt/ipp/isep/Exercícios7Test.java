package pt.ipp.isep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios7Test {

    @Test
    void maxElemMatrix() {
        Exercícios7 c = new Exercícios7();
        int result = c.MaxElemMatrix(new int[][]{{1, 2, 3}, {6, 5, 4}});
        assertEquals(6, result);
    }

    @Test
    void maxElemMatrix2() {
        Exercícios7 c = new Exercícios7();
        int result = c.MaxElemMatrix(new int[][]{{3, 2, 1, 0}, {-1, -2, -3, -4}});
        assertEquals(3, result);
    }

    @Test
    void maxElemMatrix3() {
        Exercícios7 c = new Exercícios7();
        int result = c.MaxElemMatrix(new int[][]{{1, 5, 2, 6}, {-1, 7, -3, 8}});
        assertEquals(8, result);
    }

    @Test
    void minElemMatrix1() {
        Exercícios7 c = new Exercícios7();
        int result = c.MinElemMatrix(new int[][]{{1, 2, 3}, {6, 5, 4}});
        assertEquals(1, result);
    }

    @Test
    void minElemMatrix2() {
        Exercícios7 c = new Exercícios7();
        int result = c.MinElemMatrix(new int[][]{{3, 2, 1, 0}, {-1, -2, -3, -4}});
        assertEquals(-4, result);
    }

    @Test
    void minElemMatrix3() {
        Exercícios7 c = new Exercícios7();
        int result = c.MinElemMatrix(new int[][]{{1, 5, 2, 6}, {-1, 7, -3, 8}});
        assertEquals(-3, result);
    }

    @Test
    void medElemMatrix1() {
        Exercícios7 c = new Exercícios7();
        double result = c.MedElemMatrix(new int[][]{{4, 6, 9, 20}, {8, 12, 25, 3}});
        assertEquals(10.88, result);
    }

    @Test
    void medElemMatrix2() {
        Exercícios7 c = new Exercícios7();
        double result = c.MedElemMatrix(new int[][]{{2, 6, 9, 20}, {5, 12, 25, 3}});
        assertEquals(10.25, result);
    }

    @Test
    void medElemMatrix3() {
        Exercícios7 c = new Exercícios7();
        double result = c.MedElemMatrix(new int[][]{{-2, 6, -9, 20}, {0, 12, 25, -3}});
        assertEquals(6.13, result);
    }

    @Test
    void somaLinhas() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaLinhas(new int[][]{{1, 2, 3}, {4, 5, 6}});
        assertArrayEquals(new int[]{6, 15}, result);
    }

    @Test
    void somaLinhas2() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaLinhas(new int[][]{{1, 2, 3}, {4, 5, 6}});
        assertArrayEquals(new int[]{6, 15}, result);
    }

    @Test
    void somaLinhas3() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaLinhas(new int[][]{{-2, 5, -1}, {-4, 0, 6}});
        assertArrayEquals(new int[]{2, 2}, result);
    }

    @Test
    void somaLinhas4() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaLinhas(new int[][]{{-2, 5, -1}, {-4, 0, 6}, {3, 4, 8}});
        assertArrayEquals(new int[]{2, 2, 15}, result);
    }

    @Test
    void somaColunas() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaColunas(new int[][]{{1, 2, 3}, {4, 5, 6}});
        assertArrayEquals(new int[]{5, 7, 9}, result);
    }

    @Test
    void somaColunas2() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaColunas(new int[][]{{-1, 0, -3}, {4, -5, 6}});
        assertArrayEquals(new int[]{3, -5, 3}, result);
    }

    @Test
    void somaColunas3() {
        Exercícios7 c = new Exercícios7();
        int[] result = c.somaColunas(new int[][]{{-1, 0, -3}, {4, -5, 6}, {3, 4, 9}});
        assertArrayEquals(new int[]{6, -1, 12}, result);
    }

    @Test
    void indiceMaiorSoma1() {
        Exercícios7 c = new Exercícios7();
        int result = c.indiceMaiorSoma(new int[][]{{1, 2, 3}, {4, 5, 6}});
        assertEquals(1, result);
    }

    @Test
    void indiceMaiorSoma2() {
        Exercícios7 c = new Exercícios7();
        int result = c.indiceMaiorSoma(new int[][]{{1, 2, 3}, {-4, 5, 0}, {2, 4, 9}});
        assertEquals(2, result);
    }

    @Test
    void indiceMaiorSoma3() {
        Exercícios7 c = new Exercícios7();
        int result = c.indiceMaiorSoma(new int[][]{{1, 2, 3}, {-4, 5, 0}, {-2, -4, -9}});
        assertEquals(0, result);
    }

    @Test
    void indiceMaiorSoma4() {
        Exercícios7 c = new Exercícios7();
        int result = c.indiceMaiorSoma(new int[][]{{1, 2, 3}, {-4, 5, 0}, {-2, -4, -9}, {2, 4, 9}});
        assertEquals(3, result);
    }

    @Test
    void naoNuloDiagPrincQuadrada1() {
        Exercícios7 c = new Exercícios7();
        int result = c.naoNulosDiagPrincQuadrada(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        assertEquals(3, result);
    }

    @Test
    void naoNuloDiagPrincQuadrada2() {
        Exercícios7 c = new Exercícios7();
        int result = c.naoNulosDiagPrincQuadrada(new int[][]{{0, 0, 3}, {4, 0, 6}, {7, 0, 0}});
        assertEquals(0, result);
    }

    @Test
    void naoNuloDiagPrincQuadrada3() {
        Exercícios7 c = new Exercícios7();
        int result = c.naoNulosDiagPrincQuadrada(new int[][]{{0, 0, -3}, {-4, -2, 6}, {7, 0, -6}});
        assertEquals(2, result);
    }

    @Test
    void naoNuloDiagPrincQuadrada2Teste1() {
        Exercícios7 c = new Exercícios7();
        int result = c.naoNulosDiagPrincQuadrada2(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        assertEquals(3, result);
    }

    @Test
    void naoNuloDiagPrincQuadrada2Teste2() {
        Exercícios7 c = new Exercícios7();
        int result = c.naoNulosDiagPrincQuadrada2(new int[][]{{0, 0, 3}, {4, 0, 6}, {7, 0, 0}});
        assertEquals(0, result);
    }

    @Test
    void naoNuloDiagPrincQuadrada2Teste3() {
        Exercícios7 c = new Exercícios7();
        int result = c.naoNulosDiagPrincQuadrada2(new int[][]{{0, 0, -3}, {-4, -2, 6}, {7, 0, -6}});
        assertEquals(2, result);
    }

    @Test
    void princIgualSecund1() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.princIgualSecund(new int[][]{{2, 1, 2}, {4, 1, 3}, {5, 0, 5}});
        assertEquals(true, result);
    }

    @Test
    void princIgualSecund2() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.princIgualSecund(new int[][]{{3, 1, 2}, {4, 1, 3}, {5, 0, 5}});
        assertEquals(false, result);
    }

    @Test
    void princIgualSecund3() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.princIgualSecund(new int[][]{{5, 1, 5}, {4, 2, 3}, {5, 0, 6}});
        assertEquals(false, result);
    }

    @Test
    void princIgualSecund4() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.princIgualSecund(new int[][]{{0, 1, 2, 0}, {4, 0, 0, 5}, {3, 0, 0, 3}, {8, 5, 6, 8}});
        assertEquals(true, result);
    }

    @Test
    void princIgualSecund5() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.princIgualSecund(new int[][]{{-2, 1, 2, -2}, {4, 0, 0, 5}, {3, -5, -5, 3}, {8, 5, 6, 8}});
        assertEquals(true, result);
    }

    @Test
    void somaMatrizes1() {
        Exercícios7 c = new Exercícios7();
        int[][] result = c.somaMatrizes(new int[][]{{1, 2, 3}, {4, 5, 6}}, new int[][]{{1, 2, 3}, {4, 5, 6}});
        assertArrayEquals(new int[][]{{2, 4, 6}, {8, 10, 12}}, result);
    }

    @Test
    void somaMatrizes2() {
        Exercícios7 c = new Exercícios7();
        int[][] result = c.somaMatrizes(new int[][]{{1, 0, 3}, {4, -5, 6}}, new int[][]{{0, 2, 3}, {-4, 7, 6}});
        assertArrayEquals(new int[][]{{1, 2, 6}, {0, 2, 12}}, result);
    }

    @Test
    void somaMatrizes3() {
        Exercícios7 c = new Exercícios7();
        int[][] result = c.somaMatrizes(new int[][]{{1, 0}, {4, -5}, {2, 3}, {0, 4}}, new int[][]{{0, 2}, {3, -2}, {-4, 7}, {5, 6}});
        assertArrayEquals(new int[][]{{1, 2}, {7, -7}, {-2, 10}, {5, 10}}, result);
    }

    @Test
    void produtoMatrizes1() {
        Exercícios7 c = new Exercícios7();
        int[][] result = c.produtoMatrizes(new int[][]{{1, 2}, {3, 4}}, new int[][]{{-1, 3}, {4, 2}});
        assertArrayEquals(new int[][]{{7, 7}, {13, 17}}, result);
    }

    @Test
    void produtoMatrizes2() {
        Exercícios7 c = new Exercícios7();
        int[][] result = c.produtoMatrizes(new int[][]{{2, 3}, {0, 1}, {-1, 4}}, new int[][]{{1, 2, 3}, {-2, 0, 4}});
        assertArrayEquals(new int[][]{{-4, 4, 18}, {-2, 0, 4}, {-9, -2, 13}}, result);
    }

    @Test
    void produtoMatrizes3() {
        Exercícios7 c = new Exercícios7();
        int[][] result = c.produtoMatrizes(new int[][]{{4, 3, 0, 6}, {1, 5, 2, 0}, {-1, 0, -2, 3}, {-5, -2, 0, 1}}, new int[][]{{-1, -2, 5, 2}, {-4, 2, -3, 1}, {0, 0, 3, 1}, {3, -2, 1, -1}});
        assertArrayEquals(new int[][]{{2, -14, 17, 5}, {-21, 8, -4, 9}, {10, -4, -8, -7}, {16, 4, -18, -13}}, result);
    }

    @Test
    void matrizQuadSim1() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.matrizQuadSim(new int[][]{{3, 5, 6}, {5, 2, 4}, {6, 4, 8}});
        assertEquals(true, result);
    }

    @Test
    void matrizQuadSim2() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.matrizQuadSim(new int[][]{{3, 1, 6}, {5, 2, 3}, {9, 4, 8}});
        assertEquals(false, result);
    }

    @Test
    void matrizQuadSim3() {
        Exercícios7 c = new Exercícios7();
        boolean result = c.matrizQuadSim(new int[][]{{3, 5, 6, 3}, {5, 8, 4, 6}, {6, 4, 8, 5}, {3, 6, 5, 3}});
        assertEquals(true, result);
    }

}