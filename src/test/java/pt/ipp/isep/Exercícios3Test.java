package pt.ipp.isep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios3Test {

    @Test
    public void numAlgarismos10() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismos(10);
        assertEquals(2, result);
    }
    @Test
    public void numAlgarismos400() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismos(400);
        assertEquals(3, result);
    }
    @Test
    public void numAlgarismosNegativo() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismos(-10);
        assertEquals(2, result);
    }
    @Test
    public void numAlgarismosZero() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismos(0);
        assertEquals(1, result);
    }

    @Test
    void numAlgarismosPares7() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosPares(5);
        assertEquals(0,result);
    }
    @Test
    void numAlgarismosNPares4125() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosPares(-4125);
        assertEquals(2,result);
    }
    @Test
    void numAlgarismosPares0() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosPares(0);
        assertEquals(1,result);
    }
    @Test
    void numAlgarismosImpares3011() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosImpares(3011);
        assertEquals(3, result);
    }
    @Test
    void numAlgarismosImpares5() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosImpares(5);
        assertEquals(1, result);
    }
    @Test
    void numAlgarismosImpares0() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosImpares(0);
        assertEquals(0, result);
    }
    @Test
    void numAlgarismosImparesNegativos() {
        Exercícios3 c = new Exercícios3();
        long result = c.numAlgarismosImpares(-14572);
        assertEquals(3, result);
    }
    @Test
    void mediaAlgarismosParesPos() {
        Exercícios3 c = new Exercícios3();
        double result = c.mediaAlgarismosPares(142);
        assertEquals(3, result);
    }
    @Test
    void mediaAlgarismosParesNeg() {
        Exercícios3 c = new Exercícios3();
        double result = c.mediaAlgarismosPares(-2857);
        assertEquals(5, result);
    }
    @Test
    void mediaAlgarismosPares0() {
        Exercícios3 c = new Exercícios3();
        double result = c.mediaAlgarismosPares(0);
        assertEquals(0, result);
    }
    @Test
    void mediaAlgarismosParesDouble() {
        Exercícios3 c = new Exercícios3();
        double result = c.mediaAlgarismosPares(814476);
        assertEquals(5.5, result);
    }
    @Test
    void retornarNumeroPositivo() {
        Exercícios3 c = new Exercícios3();
        long result = c.retornarNumero(1334);
        assertEquals(133, result);
    }
    @Test
    void retornarNumeroNegativo() {
        Exercícios3 c = new Exercícios3();
        long result = c.retornarNumero(-23529);
        assertEquals(359, result);
    }
    @Test
    void retornarNumero9() {
        Exercícios3 c = new Exercícios3();
        long result = c.retornarNumero(9);
        assertEquals(9, result);
    }
    @Test
    void retornarNumero4() {
        Exercícios3 c = new Exercícios3();
        long result = c.retornarNumero(4);
        assertEquals(0, result);
    }
}