package pt.ipp.isep;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercícios8Test {

    @org.junit.jupiter.api.Test
    void somaAlgarismos1() {
        Exercícios8 c = new Exercícios8();
        int result = c.somaAlgarismosVector (new int[]{1,45,7});
        assertEquals(4, result);
    }

    @org.junit.jupiter.api.Test
    void mediaAlgarismosVector1 () {
        Exercícios8 c = new Exercícios8();
        double result = c.mediaAlgarismosVector (new int[]{1,45,7});
        assertEquals(1.33, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void mediaAlgarismosVector2 () {
        Exercícios8 c = new Exercícios8();
        double result = c.mediaAlgarismosVector (new int[]{14,-2,50,0,-190,-6});
        assertEquals(1.67, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void mediaSuperiorMediaAlgarismo1 () {
        Exercícios8 c = new Exercícios8();
        int [] result = c.mediaSuperiorMediaAlgarismo (new int[]{1,45,7});
        assertArrayEquals(new int []{45}, result);
    }

    @org.junit.jupiter.api.Test
    void mediaSuperiorMediaAlgarismo2 () {
        Exercícios8 c = new Exercícios8();
        int [] result = c.mediaSuperiorMediaAlgarismo (new int[]{14,-2,50,0,-190,-6});
        assertArrayEquals(new int []{14,50,-190}, result);
    }

    @org.junit.jupiter.api.Test
    void percPares () {
        Exercícios8 c = new Exercícios8();
        double result = c.percentagemPares (1234);
        assertEquals(0.5, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void mediaPercentagemPares1 () {
        Exercícios8 c = new Exercícios8();
        double result = c.mediaPercentagemPares (new int []{12,22,33,42});
        assertEquals(0.625, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void mediaPercentagemPares2 () {
        Exercícios8 c = new Exercícios8();
        double result = c.mediaPercentagemPares(new int[]{22,32,268,851,12345});
        assertEquals(0.65, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void mediaSuperiorPercentPares1 () {
        Exercícios8 c = new Exercícios8();
        int [] result = c.mediaSuperiorPercentPares(new int []{12,22,33,42});
        assertArrayEquals(new int[]{22,42}, result);
    }

    @org.junit.jupiter.api.Test
    void mediaSuperiorPercentPares2 () {
        Exercícios8 c = new Exercícios8();
        int [] result = c.mediaSuperiorPercentPares(new int []{22,32,268,851,12345});
        assertArrayEquals(new int[]{22,268}, result);
    }

    @org.junit.jupiter.api.Test
    void elemExclAlgPares1 () {
        Exercícios8 c = new Exercícios8();
        int [] result = c.elemExclAlgPares(new int []{-22,32,268,0,851,-2,12345});
        assertArrayEquals(new int[]{-22,268,0,-2}, result);
    }

    @org.junit.jupiter.api.Test
    void isCrecent () {
        Exercícios8 c = new Exercícios8();
        boolean result = c.isCrescent(new long []{2,2});
        assertEquals(false, result);
    }

    @org.junit.jupiter.api.Test
    void elemSeqCresc () {
        Exercícios8 c = new Exercícios8();
        int [] result = c.elemSeqCresc(new int []{-15,22,32,1,-59,268,9,851,-54321,0,451});
        assertArrayEquals(new int[]{-15,-59,268}, result);
    }

    @org.junit.jupiter.api.Test
    void capicuaVector() {
        Exercícios8 c = new Exercícios8();
        int [] result = c.capicuaVector(new int []{15,-222,32,1,-5995,268,9,0,8118,-54321,0,4884});
        assertArrayEquals(new int[]{-222,1,-5995,9,0,8118,0,4884}, result);
    }

    @org.junit.jupiter.api.Test
    void notArmstrongVector() {
        Exercícios8 c = new Exercícios8();
        int [] result = c.notArmstrongVector(new int []{2,22,153,-1234,370,0,371,-89,564,407});
        assertArrayEquals(new int[]{22,-1234,-89,564}, result);
    }

    @org.junit.jupiter.api.Test
    void elemMesmoAlgarismo() {
        Exercícios8 c = new Exercícios8();
        int [] result = c.elemMesmoAlgarismo(new int []{22,153,-5555,370,0,777,-89,564,44444,9,66});
        assertArrayEquals(new int[]{22,-5555,0,777,44444,9,66}, result);
    }

    @Test
    @DisplayName("Retornar vector com os elementos que são sequências crescentes")
    void elemSeqCrescAlgNGeral() {
        Exercícios8 c = new Exercícios8();
        int nrAlg = 2;
        int [] result = c.elemSeqCrescAlgN(nrAlg, new int[]{82,17,39,93});
        assertArrayEquals(new int[]{17,39}, result);
    }

    @Test
    @DisplayName("Retornar vector com elementos que são sequências crescentes cujo vector dado como argumento tem números positivos com vários algarismo")
    void elemSeqCrescAlgNPositiveNumbersWithSeveralDigits () {
        Exercícios8 c = new Exercícios8();
        int nrAlg = 3;
        int [] result = c.elemSeqCrescAlgN(nrAlg, new int[]{9,3,39,42,159,325,3489});
        assertArrayEquals(new int[]{159,3489}, result);
    }

    @Test
    @DisplayName("Retornar vector com elementos que são sequências crescentes cujo vector dado como argumento tem números negativos com vários algarismo")
    void elemSeqCrescAlgNNegativeNumbersWithSeveralDigits () {
        Exercícios8 c = new Exercícios8();
        int nrAlg = 2;
        int [] result = c.elemSeqCrescAlgN(nrAlg, new int[]{-2,-17,-85,-629,-4789});
        assertArrayEquals(new int[]{-17,-629,-4789}, result);
    }

    @Test
    @DisplayName("Retornar vector com elementos que são sequências crescentes cujo vector dado como argumento tem números positivos e negativos com vários algarismo")
    void elemSeqCrescAlgNPositivesNegativeNumbersWithSeveralDigits () {
        Exercícios8 c = new Exercícios8();
        int nrAlg = 2;
        int [] result = c.elemSeqCrescAlgN(nrAlg, new int[]{-2,25,-17,1,-85,58,-629,345,-4789});
        assertArrayEquals(new int[]{25,-17,58,-629,345,-4789}, result);
    }

    @Test
    @DisplayName("Retornar vector com elementos que são sequências crescentes cujo vector dado como argumento tem números nulos")
    void elemSeqCrescAlgNNullNumbers () {
        Exercícios8 c = new Exercícios8();
        int nrAlg = 2;
        int [] result = c.elemSeqCrescAlgN(nrAlg, new int[]{15,0,52,-38,0,-452,258,0,8723});
        assertArrayEquals(new int[]{15,-38,-452,258,8723}, result);
    }

    @Test
    @DisplayName("Retornar vector com 0 uma vez que um algarismo não é uma sequência")
    void elemSeqCrescAlgNOneAlgarism () {
        Exercícios8 c = new Exercícios8();
        int nrAlg = 1;
        int [] result = c.elemSeqCrescAlgN(nrAlg, new int[]{15,0,52,-38,0,-452,258,0,8723});
        assertArrayEquals(new int[]{0}, result);
    }
}