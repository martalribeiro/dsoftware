package pt.ipp.isep;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercícios1Test {

    @org.junit.jupiter.api.Test
    void min() {
        Exercícios1 c = new Exercícios1();
        int result = c.min(1,9);
        assertEquals ( 1, result);
    }

    @org.junit.jupiter.api.Test
    void max() {
        Exercícios1 c = new Exercícios1();
        int result = c.max(1,9);
        assertEquals ( 9, result);
    }

    @Test
    void abs() {
        Exercícios1 c = new Exercícios1();
        int result = c.abs(-5);
        assertEquals ( 5, result);
    }

    @Test
    void inc() {
        Exercícios1 c = new Exercícios1();
        int result = c.inc(2);
        assertEquals ( 3, result);
    }

    @Test
    void dec() {
        Exercícios1 c = new Exercícios1();
        int result = c.dec(2);
        assertEquals ( 1, result);
    }

    @Test
    void testMax() {
        Exercícios1 c = new Exercícios1();
        int result = c.max3(2,4,6);
        assertEquals (6, result);
    }

    @Test
    void max31() {
        Exercícios1 c = new Exercícios1();
        int result = c.max31(2,4,6);
        assertEquals (6, result);
    }

    @Test
    void multiplos2() {
        Exercícios1 c = new Exercícios1();
        boolean result = c.multiplos2(12);
        assertEquals( true, result);
    }


    //INICIO DO CICLO DE EXERCÍCIOS
    @Test
    @DisplayName("Testar multiplo de 3")
    public void eMult3() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.eMult3(13);
        boolean expected = true;
        assertEquals( false, actual);
    }
    @Test
    @DisplayName("Testar multiplo de 3")
    public void eMult32() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.eMult3(15);
        boolean expected = true;
        assertEquals( true, actual);
    }

    @Test
    public void contMult3() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMult3(1,20);
        int expected = 6;
        assertEquals(6, conta);
    }
    @Test
    public void contMult32() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMult3(20,1);
        int expected = 6;
        assertEquals(6, conta);
    }
    @Test
    public void contMult33() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMult3(20,19);
        assertEquals(0, conta);
    }
    @Test
    public void contMult34() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMult3(0,5);
        assertEquals(2, conta);
    }
    @Test
    public void contMult35() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMult3(12,12);
        int expected = 1;
        assertEquals(1, conta);
    }
    @Test
    void multiplo() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.multiplo(12,3);
        assertEquals( true, actual);
    }

    @Test
    void multiplo1() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.multiplo(21,3);
        assertEquals( true, actual);
    }

    @Test
    void multiplo2() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.multiplo(11,3);
        assertEquals( false, actual);
    }
    @Test
    void multiplo3() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.multiplo(-12,-3);
        assertEquals( true, actual);
    }
    @Test
    void multiplo4() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.multiplo(3,0);
        assertEquals( false, actual);
    }
    @Test
    void multiplo5() {
        Exercícios1 c = new Exercícios1();
        boolean actual = c.multiplo(0,3);
        assertEquals( true, actual);
    }
    @Test
    void contMultiplo() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMultiplo(3,10,3);
        int expected = 3;
        assertEquals(3, conta);
    }
    @Test
    void contMultiplo2() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMultiplo(10,3,3);
        int expected = 3;
        assertEquals(3, conta);
    }
    @Test
    void contMultiplo3() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMultiplo(8,12,7);
        assertEquals(0, conta);
    }
    @Test
    void contMultiplo4() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMultiplo(-4,10,-3);
        assertEquals(5, conta);
    }
    @Test
    void contMultiplo5() {
        Exercícios1 x = new Exercícios1();
        int conta = x.contMultiplo(4,20,0);
        assertEquals(-1, conta);
    }
    @Test
    public void contaMult35() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaMult35(-2,15);
        assertEquals(2, result);
    }
    @Test
    public void contaMult35Teste2() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaMult35(30,5);
        assertEquals(2, result);
    }
    @Test
    public void contaMult35Teste3() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaMult35(10,5);
        assertEquals(0, result);
    }
    @Test
    public void anyMult() {
        Exercícios1 x = new Exercícios1();
        boolean result = x.anyMult(0,2,5);
        assertEquals(true, result);
    }
    @Test
    public void contaAnyMult() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaAnyMult(2,14,2,3);
        assertEquals(2, result);
    }
    @Test
    public void contaAnyMult2() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaAnyMult(4,14,7,3);
        assertEquals(0, result);
    }
    @Test
    public void contaAnyMult3() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaAnyMult(14,2,2,3);
        assertEquals(2, result);
    }
    @Test
    public void contaAnyMult4() {
        Exercícios1 x = new Exercícios1();
        int result = x.contaAnyMult(4,14,0,3);
        assertEquals(-1, result);
    }
    @Test
    public void addAnyMult1() {
        Exercícios1 x = new Exercícios1();
        int result = x.addAnyMult(1,12,2,3);
        assertEquals(18,result);
    }
    @Test
    public void addAnyMult2() {
        int result = Exercícios1.addAnyMult(-4,26,11,3);
        assertEquals(0,result);
    }
    @Test
    public void addAnyMult3() {
        int result = Exercícios1.addAnyMult(12,1,2,3);
        assertEquals(18,result);
    }
    @Test
    public void addAnyMult4() {
        int result = Exercícios1.addAnyMult(12,1,0,3);
        assertEquals(-1,result);
    }

    //Testes dos Exercícios 2

    @Test
    public void somaMultNum() {
        Exercícios1 c = new Exercícios1();
        long result = c.somaMultNum(5,11,0);
        assertEquals(15, result);
    }
    @Test
    public void prodMultNum() {
        Exercícios1 c = new Exercícios1();
        long result = c.prodMultNum(2,1,5);
        assertEquals(8,result);
    }
    @Test
    public void prodMultNum2() {
        Exercícios1 c = new Exercícios1();
        long result = c.prodMultNum(5,1,4);
        assertEquals(0,result);
    }

    @Test
    public void contaMultNum () {
        Exercícios1 c = new Exercícios1();
        long result = c.contaMultNum(2,5,1);
        assertEquals(2, result);
    }
    @Test
    public void medMultNum () {
        Exercícios1 c= new Exercícios1();
        double result = c.medMultNum(3,2,7);
        assertEquals(4.5,result);
    }
}
