package pt.ipp.isep;

//II. Para cada uma das alíneas seguintes elabore um método que retorne:
//a. a soma de todos os números pares num intervalo dado: long somaPares(int inf, int sup)
//b. a soma de todos os números múltiplos de um dado número num intervalo definido por dois números: long somaMultNum(int num, int a, int b). Os dois números, que definem os limites do intervalo, não estão necessariamente por ordem crescente.
//c. o produto de todos os números múltiplos de um dado número num intervalo definido por dois números.
//d. o número de números múltiplos de um dado número num intervalo definido por dois números (igual a I.b. - ignorar).
//e. a média dos múltiplos de um dado número num intervalo definido por dois números.

public class Exercícios2 {
    //Exercise II
    //a.
    public boolean pares(int nr1) {
        int x = nr1 % 2;
        if (x==0) {
            return true;
        }
        return false;
    }

    public long somaPares(int inf, int sup) {
        long soma=0;
        boolean x;
        int l3 = 0;
        if (inf>sup) {
            l3=inf;
            inf=sup;
            sup=l3;
        }
        for (int I = inf; I<=sup; I++) {
            x = pares(I);
            if (x==true) {
                soma = soma + I;
            }
        }
        return soma;
    }
}
