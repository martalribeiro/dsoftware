package pt.ipp.isep;

public class Exercícios1 {
    public int min(int nr1, int nr2) {
        if (nr1 > nr2) return nr2;
        return nr1;
    }

    public int max(int nr1, int nr2) {
        if (nr1 < nr2) return nr2;
        return nr1;
    }

    public int abs(int nr1) {
        if (nr1 >= 0) return nr1;
        return (-1) * nr1;
    }

    public int inc(int nr1) {
        return nr1 + 1;
    }

    public int dec(int nr1) {
        return nr1 - 1;
    }

    public int max3(int nr1, int nr2, int nr3) {
        int m;
        if (nr1 > nr2 && nr1 > nr3) m = nr1;
        else if (nr1 > nr2 && nr1 < nr3) m = nr3;
        else m = nr3;
        return m;
    }

    public int max31(int nr1, int nr2, int nr3) {
        return max(nr1, max(nr2, nr3));
    }

    public boolean multiplos2(int nr1) {
        if (nr1 % 2 == 0) return true;
        return false;
    }

    /* EXERCICIOS10:
     */
// Ciclos
//I. Sabendo que o operador % do Java dá o resto da divisão entre dois inteiros pretende-se que elabore para cada alínea um método que:
//a. retorne o número de múltiplos de 3 num intervalo dado.
//b. retorne o número de múltiplos de um dado número inteiro num intervalo dado.
//c. retorne o número de múltiplos de 3 e 5 num intervalo dado.
//d. retorne o número de múltiplos de dois números inteiros num intervalo dado.
//e. retorne a soma dos múltiplos de dois números inteiros num intervalo dado.
    //exercício I.a
    public boolean eMult3(int n) {
        int x = n % 3;
        if (x == 0) {
            return true;
        }
        return false;
    }

    public int contMult3(int l1, int l2) {
        int conta = 0;
        boolean x;
        int l3 = 0;
        if (l1>=l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int I = l1; I <= l2; I++) {
            x = eMult3(I);
            if (x == true) {
                conta = conta + 1;
            }
        }
        return conta;
    }

    //exercicio I.b
    public boolean multiplo(int a, int b) {
        if (b == 0) {
            return false;
        }
        int x = a % b;
        if (x == 0) {
            return true;
        }
        return false;
    }

    public int contMultiplo(int l1, int l2, int b) {
        if(b == 0)
            return -1;
        int conta = 0;
        boolean x;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int A = l1; A <= l2; A++) {
            x = multiplo(A, b);
            if (x == true) {
                conta = conta + 1;
            }
        }
        return conta;
    }

    //exercicio I.c
    public boolean multiplo35(int nr1) {
        int x = nr1 % 3;
        int y = nr1 % 5;
        if (x == 0 && y == 0) {
            return true;
        }
        return false;
    }

    public int contaMult35(int l1, int l2) {
        int conta = 0;
        boolean x;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int I = l1; I <= l2; I++) {
            x = multiplo35(I);
            if (x == true) {
                conta = conta + 1;
            }
        }
        return conta;
    }

    //exercicio I.d/e
    public static boolean anyMult(int nr1, int a, int b) {
        int x = nr1 % a;
        int y = nr1 % b;
        if (x == 0 && y == 0) {
            return true;
        }
        return false;
    }

    public int contaAnyMult(int l1, int l2, int a, int b) {
        if(a == 0 || b == 0)
            return -1;
        int conta = 0;
        boolean x;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int I = l1; I <= l2; I++) {
            x = anyMult(I, a, b);
            if (x == true) {
                conta = conta + 1;
            }
        }
        return conta;
    }

    public static int addAnyMult(int l1, int l2, int a, int b) {
        if(a == 0 || b == 0)
            return -1;
        int add = 0;
        boolean x;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int I = l1; I <= l2; I++) {
            x = anyMult(I, a, b);
            if (x == true) {
                add = add + I;
            }
        }
        return add;
    }
    //II. Para cada uma das alíneas seguintes elabore um método que retorne:
//a. a soma de todos os números pares num intervalo dado: long somaPares(int inf, int sup)
//b. a soma de todos os números múltiplos de um dado número num intervalo definido por dois números: long somaMultNum(int num, int a, int b).
// Os dois números, que definem os limites do intervalo, não estão necessariamente por ordem crescente.
//c. o produto de todos os números múltiplos de um dado número num intervalo definido por dois números.
//d. o número de números múltiplos de um dado número num intervalo definido por dois números (igual a I.b. - ignorar).
//e. a média dos múltiplos de um dado número num intervalo definido por dois números.

    //II.b
    public long somaMultNum(int num, int a, int b) {
        long soma = 0;
        boolean x;
        if (a <= b) {
            for (int I = a; I <= b; I++) {
                x = multiplo(I, num);
                if (x == true) {
                    soma = soma + I;
                }
            }
            return soma;
        }
        else if (b <= a) {
            for (int I = a; I >= b; I--) {
                x = multiplo(I, num);
                if (x == true) {
                    soma = soma + I;
                }
            }
            return soma;
        }
        return soma;
    }
    //II.c
    public long prodMultNum (int num, int a, int b) {
        long prod = 1;
        boolean x = false;
        int c=0;
        if (a>=b) {
            c=a;
            a=b;
            b=c;
        }
        if (a<=b) {
            for (int I=a; I<b; I++) {
                x=multiplo(I,num);
                if (x==true) {
                    prod = prod * I;
                }
            }
        }
        if (prod>1) {
            return prod;
        }
        else
            return 0;
    }

    //II.d
    public long contaMultNum (int nr1, int a, int b) {
        long conta=0;
        boolean x;
        int c=0;
        if (a>=b) {
            c=a;
            a=b;
            b=c;
        }
        for (int I=a; I<=b; I++) {
            x=multiplo(I,nr1);
            if (x==true) {
                conta=conta+1;
            }
        }
        return conta;
    }
    //II.e
    public double medMultNum (int nr1, int a, int b) {
        double med = 0;
        int conta=0;
        boolean x;
        int c=0;
        if (a>=b) {
            c=a;
            a=b;
            b=c;
        }
        for (int I=a; I<=b; I++) {
            x=multiplo(I,nr1);
            if (x==true) {
                med=med+I;
                conta=conta+1;
            }
        }
        if(conta>0) {
            return med / (double) conta;
        } else
            return Double.NaN;
    }
}
