package pt.ipp.isep;

/*V. Considerandos as funções desenvolvidas no grupo de exercícios I pretende-se que elabore para cada alínea um método que:
a. retorne os múltiplos de 3 num intervalo dado.
b. retorne os múltiplos de um dado número inteiro num intervalo dado.
c. retorne os múltiplos de 3 e 5 num intervalo dado.
d. retorne os múltiplos de dois números inteiros num intervalo dado.

 */


public class Exercícios5 {

    //V.a
    public int[] retornarMultiplo3 (int l1, int l2) {
        int c = 0;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int i=l1; i<=l2; i++) {
            if (i%3==0) {
                c++;
            }
        }
        int[] multiplo3 = new int[c];
        int a = 0;
        while (l1<=l2) {
            if (l1 % 3 == 0) {
                multiplo3[a] = l1;
                a++;
            }
            l1++;
        }
        return multiplo3;
    }

    //V.b
    public int[] retornarMultiploDesc (int a, int l1, int l2) {
        if (a==0) {
            return new int[]{-1};
        }
        int c = 0;
        int l3 = 0;
        int[] multArray;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        for (int i=l1; i<=l2; i++) {
            if (i%a==0) {
                c++;
            }
        }
        multArray = new int[c];
        int b = 0;
        for (int j=l1; j<=l2; j++) {
            if (j%a==0) {
                multArray[b]=j;
                b++;
            }
        }
        return multArray;
    }

    //V.c
    public int[] retornarMultiplo35(int l1, int l2) {
        int a = 0;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        int[] mult35;
        for (int i=l1; i<=l2; i++) {
            if (i%3==0 && i%5==0) {
                a++;
            }
        }
        mult35 = new int[a];
        int b = 0;
        for (int j = l1; j<=l2; j++) {
            if (j%3==0 && j%5==0) {
                mult35[b]=j;
                b++;
            }
        }
        return mult35;
    }

    //V.d
    public int[] retornarMultiplos2Desc (int a, int b, int l1, int l2) {
        if (a==0) {
            return new int[]{-1};
        }
        int conta = 0;
        int l3 = 0;
        if (l1>l2) {
            l3=l1;
            l1=l2;
            l2=l3;
        }
        int[] mult2Desc;
        for (int i=l1; i<=l2; i++) {
            if (i%a==0 && i%b==0) {
                conta++;
            }
        }
        mult2Desc = new int[conta];
        int c = 0;
        for (int j=l1; j<=l2; j++) {
            if (j % a == 0 && j % b == 0) {
                mult2Desc[c] = j;
                c++;
            }
        }
        return mult2Desc;
    }
}
