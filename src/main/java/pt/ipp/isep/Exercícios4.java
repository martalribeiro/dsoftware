package pt.ipp.isep;

public class Exercícios4 {

    /*
    IV. Para cada uma das alíneas seguintes elabore um método que:
a. retorne a média dos algarismos de um número inteiro longo: float medAlgarismo( long num).
b. retorne a soma dos algarismos de um número inteiro longo.
c. retorne o “simétrico” de um número inteiro longo, i.e. o número inteiro longo que corresponde à leitura do número original da direita para a esquerda.
d. verifique se um número inteiro longo é capicua: boolean isCapicua( long numero).
e. verifique se um número é número de Amstrong, i.e. se for igual à soma dos cubos dos seus algarismos.
f. retorne a primeira capicua num intervalo dado.
g. retorne a maior capicua num intervalo dado.
h. retorne o número de capicuas num intervalo dado.
i. retorne o primeiro número de Amstrong num intervalo dado.
j. retorne o número de números de Amstrong num intervalo dado.

     */

    //IV.a
    public float medAlgarismoLongo (long num) {
        if (num==0) {
            return -1;
        }

        int i = 0;
        long soma = 0;
        if (num<0) {
            num = num*(-1);
        }
        while (num>0) {
            long x = num%10;
            i++;
            soma = soma + x;
            num = num/10;
        }
        return (float) soma/i;
    }

    //IV.b
    public long somaAlgarismoLongo (long num) {
        if (num==0) {
            return -1;
        }

        int i = 0;
        long soma = 0;
        if (num<0) {
            num = num * (-1);
        }
        while (num>0) {
            long x = num%10;
            i++;
            soma = soma + x;
            num = num/10;
        }
        return soma;
    }

    //IV.c
    public long simNumeroLongo(long num) {
        long alg = 0;
        double x = 0;
        if (num<0) {
            num = num * (-1);
        }
        while (num>0) {
            alg = num % 10;
            x = x*10 + alg;
            num = num / 10;
        }
        if (num==0) {
            return (long) x;
        }
        return (long) x;
    }

    //IV.d
    public boolean isCapicua (long num) {
        if (num<0) {
            num = num * (-1);
        }
        long x = simNumeroLongo(num);
        if(num==x) {
            return true;
        }
        else return false;
    }

    //IV.e
    public static boolean isArmstrong (long num) {
        int i = 0;
        int g = 0;
        long soma = 0;
        long inicial = num;
        long x = 0;
        if (num<0) {
            num = num * (-1);
        }
        while (num>0) {
            num = num / 10;
            i++;
        }
        num = inicial;
        while (g<i) {
            long a = num%10;
            soma = soma + (long) Math.pow(a, i);
            num = num / 10;
            g++;
        }
        if (soma==inicial) {
            return true;
        }
        else return false;
    }

    //IV.f
    public int firstCapicua (int a, int b) {
        boolean x;
        int i = 0;
        if (a > b) {
            for (i = a; i >b ; i--) {
                x = isCapicua(i);
                if (x == true) {
                    return i;
                }
            }
        }
        if (b > a) {
            for (i = a; i < b; i++) {
                x = isCapicua(i);
                if (x == true) {
                    return i;
                }
            }
        }
        return i;
    }

    //IV.g
    public int greaterCapicua (int a, int b) {
        boolean x;
        int i = 0;
        if (b > a) {
            for (i = b; i > a ; i--) {
                x = isCapicua(i);
                if (x == true) {
                    return i;
                }
            }
        }
        if (a > b) {
            for (i = a; i > b ; i--) {
                x = isCapicua(i);
                if (x == true) {
                    return i;
                }
            }
        }
        return i;
    }

    //IV.h
    public int contaCapicua(int a, int b) {
        int conta = 0;
        int i;
        boolean x;
        int c = 0;
        if (a>b) {
            c=a;
            a=b;
            b=c;
        }
        for (i=a; i<b; i++) {
            x = isCapicua(i);
            if (x==true) {
                conta++;
            }
        }
        return conta;
    }

    //IV.i
    public int firstArmstrong (int a, int b) {
        boolean x;
        int c = 0;
        int i = 0;
        if (a>b) {
            for (i=a; i>b ; i--) {
                x = isArmstrong(i);
                if (x==true) {
                    return i;
                }
            }
        }
        if (b>a) {
            for (i = a; i < b; i++) {
                x = isArmstrong(i);
                if (x == true) {
                    return i;
                }
            }
        }
        return i;
    }

    //IV.j
    public int contaArmstrong (int a, int b) {
        boolean x;
        int conta = 0;
        int c = 0;
        if (b<=a) {
            c=a;
            a=b;
            b=c;
        }
        for (int i=a; i<=b; i++) {
            x = isArmstrong(i);
            if (x==true) {
                conta++;
            }
        }
        return conta;
    }
}
