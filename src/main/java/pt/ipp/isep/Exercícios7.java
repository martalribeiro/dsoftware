package pt.ipp.isep;

/*
VII. Para cada uma das alíneas seguintes elabore um método que retorne:
a) o maior elemento de uma matriz
b) o menor elemento de uma matriz
c) a média dos elementos de uma matriz
d) um vetor em que cada posição corresponde à soma dos elementos da linha homóloga de uma matriz
e) um vetor em que cada posição corresponde à soma dos elementos da coluna homóloga de uma matriz
f) o índice da linha da matriz com maior soma dos respetivos elementos. Deve ser usado o método da alínea d).
g) o número de elementos não nulos na diagonal principal de uma matriz quadrada
h) true caso a diagonal principal e a secundária sejam iguais, i.e. tenham os mesmos elementos e pela mesma ordem
i) uma matriz que é a soma de duas matrizes passadas como argumento
j) uma matriz que é o produto de duas matrizes passadas como argumento
k) True se uma matriz quadrada é simétrica

 */
public class Exercícios7 {

    //VII.a
    public int MaxElemMatrix(int[][] matrix) {
        int l = matrix.length;
        int c = matrix[0].length;
        int v1 = matrix[0][0];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                if (v1 < matrix[i][j]) {
                    v1 = matrix[i][j];
                }
            }
        }
        return v1;
    }

    //VII.b
    public int MinElemMatrix(int[][] matrix) {
        int l = matrix.length;
        int c = matrix[0].length;
        int v1 = matrix[0][0];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                if (v1 > matrix[i][j]) {
                    v1 = matrix[i][j];
                }
            }
        }
        return v1;
    }

    //VII.c
    public double MedElemMatrix(int[][] matrix) {
        int l = matrix.length;
        int c = matrix[0].length;
        int soma = 0;
        int conta = 0;
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                soma = soma + matrix[i][j];
                conta++;
            }
        }
        double media = (double) soma / conta;
        double result = (double) Math.round(media * 100) / 100;
        return result;
    }

    //VII.d
    public int[] somaLinhas(int[][] matrix) {
        int lnh = matrix.length;
        int col = matrix[0].length;
        int soma = 0;
        int[] vector = new int[lnh];
        for (int i = 0; i < lnh; i++) {
            for (int j = 0; j < col; j++) {
                soma = soma + matrix[i][j];
            }
            vector[i] = soma;
            soma = 0;
        }
        return vector;
    }

    //VII.e
    public int[] somaColunas(int[][] matrix) {
        int lnh = matrix.length;
        int col = matrix[0].length;
        int soma = 0;
        int[] vector = new int[col];
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < lnh; j++) {
                soma = soma + matrix[j][i];
            }
            vector[i] = soma;
            soma = 0;
        }
        return vector;
    }

    //VII.f
    public int indiceMaiorSoma(int[][] matrix) {
        int[] array = somaLinhas(matrix);
        int lnh = array.length;
        int ind = lnh - 1;
        for (int i = 0; i < lnh - 1; i++) {
            if (array[ind] < array[ind - 1]) {
                ind = ind - 1;
            }
        }
        return ind;
    }

    //VII.g
    public int naoNulosDiagPrincQuadrada(int[][] matrix) { //este método está a correr desnecessariamente a matriz toda,
        int lnh = matrix.length;// podemos apenas correr um array, o da diagonal principal, ver resolução seguinte
        int col = matrix[0].length;
        int conta = 0;
        for (int i = 0; i < lnh; i++) {
            for (int j = 0; j < col; j++) {
                if (i == j && matrix[i][j] != 0) {
                    conta++;
                }
            }
        }
        return conta;
    }

    //VII.g método alternativo mais eficaz
    public int naoNulosDiagPrincQuadrada2(int[][] matrix) {
        int [] array = new int[matrix.length];
        int conta = 0;
        for (int i=0; i<matrix.length; i++) {
            array[i] = matrix[i][i];
            if (array[i] != 0) {
                conta++;
            }
        }
        return conta;
    }

    //VII.h
    public boolean princIgualSecund(int[][] matrix) {
        int lnh = matrix.length;
        int col = matrix[0].length;
        int ind1 = 0;
        for (int i = 0; i < lnh; i++) {
            for (int j = 0; j < col; j++) {
                if (matrix[ind1][ind1] != matrix[ind1][col - 1]) {
                    return false;
                }
                if (matrix[ind1][ind1] == matrix[ind1][col - 1]) {
                    ind1++;
                    col--;
                }
            }
        }
        return true;
    }


    //VII.i
    public int[][] somaMatrizes(int[][] matrix1, int[][] matrix2) {
        int lnh1 = matrix1.length;
        int col1 = matrix1[0].length;
        int[][] matrix3 = new int[lnh1][col1];
        for (int i = 0; i < lnh1; i++) {
            for (int j = 0; j < col1; j++) {
                int soma = matrix1[i][j] + matrix2[i][j];
                matrix3[i][j] = soma;
                soma = 0;
            }
        }
        return matrix3;
    }

    //VII.j
    public int[][] produtoMatrizes(int[][] matrix1, int[][] matrix2) {
        int lnh1 = matrix1.length;
        int lnh2 = matrix2.length;
        int col2 = matrix2[0].length;
        int[][] matrix3 = new int[lnh1][col2];
        int soma = 0;
        for (int i = 0; i < lnh1; i++) {
            for (int j = 0; j < col2; j++) {
                for (int k = 0; k < lnh2; k++) { // o k vai dar o indice para fazer a adição da multiplicação em multiplicacão de matrizes,
                    soma = soma + matrix1[i][k] * matrix2[k][j]; //numa linha corre as colunas necessárias;
                }
                matrix3[i][j] = soma;
                soma = 0;
            }
        }
        return matrix3;
    }

    //VII.k
    public boolean matrizQuadSim(int[][] matrix) {
        int lnh = matrix.length;
        int col = matrix[0].length;
        for (int i = 0; i < lnh; i++) {
            for (int j = 0; j < col; j++) {
                if (matrix[i][j] != matrix[j][i]) {
                    return false;
                }
            }
        }
        return true;
    }

}
