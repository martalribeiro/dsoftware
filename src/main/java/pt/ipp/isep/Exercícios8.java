package pt.ipp.isep;

import java.util.Arrays;

public class Exercícios8 {

    //VIII - Considerandos os métodos desenvolvidos nos conjuntos de exercícios anterior, pretende-se que elaborem para cada alínea um método que:
    //a.Retorne os elementos de um vetor de números inteiros cujo número de algarismos é superior ao número médio de algarismos de todos os elementos do vetor.
    //b.Retorne os elementos de um vetor de números inteiros cuja percentagem de algarismos pares é superior à média da percentagem de algarismos pares de todos os elementos do vetor.
    //c.Retorne os elementos compostos exclusivamente por algarismos pares de um vetor de números inteiros.
    //d.Retorne os elementos que são sequências crescentes (e.g. 347) de um vetor de números inteiros.
    //e.Retorne as capicuas existentes num vetor de números inteiros.
    //f.Retorne os números compostos exclusivamente por um mesmo algarismo (e.g. 222) existentes num vetor de números inteiros.
    //g.Retorne os números que não são de Amstrong existentes num vetor de números inteiros.
    //h.Retorne os elementos que contêm uma sequência crescente de pelo menos n algarismos (e.g. n=3, 347) de um vetor de números inteiros.

    //VIII.a

    public static int somaAlgarismosVector (int [] vector1) {
        int soma = 0;
        int compVector = vector1.length;
        for (int i = 0; i<compVector; i++) {
            int contaAlgarismos = FunctionLibrary.numAlgarismos(vector1[i]);
            soma = soma + contaAlgarismos;
        }
        return soma;
    }

    public static double mediaAlgarismosVector(int[] vector2) {
        int somaAlgarismos = somaAlgarismosVector(vector2);
        int nrNumerosArray = vector2.length;
        double media = (double) somaAlgarismos/nrNumerosArray;
        return media;
    }

    public static int [] mediaSuperiorMediaAlgarismo (int [] vector3) {
        if(vector3.length <= 1)
            return new int[0];
        double media = mediaAlgarismosVector(vector3);
        int compvector3 = vector3.length;
        int [] vectorAlgSupMedia = new int [0];
        int countVectorAlgSupMedia = 0;
        int contaAlg = 0;
        for (int i = 0; i<compvector3; i++) {
            contaAlg = FunctionLibrary.numAlgarismos(vector3[i]);
            if (contaAlg > media) {
                countVectorAlgSupMedia++;
                vectorAlgSupMedia = aumentarNrElementosVectorInt(vectorAlgSupMedia, vector3,i,countVectorAlgSupMedia);
            }
        }
        if (vectorAlgSupMedia.length < 0) {
            return new int[0];
        }
        else return vectorAlgSupMedia;
    }

    public static int [] aumentarNrElementosVectorInt(int[] vector2, int[] vector1, int i, int nrVector1) {
        //aumenta o nr de espaços vazios do vector e insere o próximo elemento desejado
        vector2 = Arrays.copyOf(vector2, nrVector1);
        vector2[nrVector1-1] = vector1[i];
        return vector2;
    }

    //VIII.b

    public static double percentagemPares (long num) {
        long algPares = FunctionLibrary.numAlgarismosPares(num);
        long countAlgarismos = FunctionLibrary.numAlgarismos(num);
        double percPares = (double) algPares/countAlgarismos;
        return percPares;
    }

    public static double mediaPercentagemPares (int [] vector1) {
        double somaPercPares = 0;
        int compVector1 = vector1.length;
        int contaNum = 0;
        for (int i = 0; i<compVector1; i++) {
            double percPares = percentagemPares(vector1[i]);
            contaNum++;
            somaPercPares = somaPercPares + percPares;
        }
        double mediaPercentPares = somaPercPares/contaNum;
        return mediaPercentPares;
    }

    public static int [] mediaSuperiorPercentPares (int [] vector3) {
        if(vector3.length <= 1)
            return new int[0];
        double media = mediaPercentagemPares(vector3);
        int compvector3 = vector3.length;
        int [] vectorAlgSupMedia = new int [0];
        int countVectorAlgSupMedia = 0;
        for (int i = 0; i<compvector3; i++) {
            if (percentagemPares(vector3[i]) > media) {
                countVectorAlgSupMedia++;
                vectorAlgSupMedia = aumentarNrElementosVectorInt(vectorAlgSupMedia, vector3,i,countVectorAlgSupMedia);
            }
        }
        if (vectorAlgSupMedia.length < 0) {
            return new int[0];
        }
        else return vectorAlgSupMedia;
    }

    //VIII.c

    public static int [] elemExclAlgPares (int [] vector1) {
        if(vector1.length <= 1)
            return new int[0];
        int [] algPares = new int[0];
        int nrAlgPares = 0;
        int compVector1 = vector1.length;
        for (int i=0; i <compVector1; i++) {
            double percPares = percentagemPares(vector1[i]);
            if (percPares == 1) {
                nrAlgPares++;
                algPares = aumentarNrElementosVectorInt(algPares, vector1,i,nrAlgPares);
            }
        }
        if (nrAlgPares == 0) {
            return new int[0];
        } else
            return algPares;
    }

    //VIII.d

    public static int[] elemSeqCresc (int[] arrayCrescent) {
        int arrayLenght = arrayCrescent.length;
        int [] SeqCresc = new int [0];
        int nrSeqCresc = 0;
        for ( int i = 0; i<arrayLenght; i++) {
            if (Math.abs(arrayCrescent[i]) > 10 ) {
                long[] extract = extrairAlgParaArray(arrayCrescent[i]);
                boolean x = isCrescent(extract);
                if (x == true) {
                    nrSeqCresc++;
                    SeqCresc = Arrays.copyOf(SeqCresc, nrSeqCresc);
                    SeqCresc[nrSeqCresc - 1] = arrayCrescent[i];
                }
            }
        }
        if (nrSeqCresc == 0) {
            return new int[]{0};
        } else
            return SeqCresc;
    }

    public static long [] extrairAlgParaArray (long num) {
        int count = FunctionLibrary.numAlgarismos(num);
        long [] arrayAlgarismo = new long [count];
        int i = count-1;
        while ( num != 0) {
            num = Math.abs(num);
            long algarismo = num % 10;
            arrayAlgarismo[i] = algarismo;
            i--;
            num = num/10;
        }
        return arrayAlgarismo;
    }

    public static boolean isCrescent (long [] array){
        for (int j = 0; j < array.length-1; j++) {
            if (array[j] >= array[j + 1]) {
                return false;
            }
        }
        return true;
    }

    //VIII.e

    public static int [] capicuaVector (int [] vector1) {
        int compVector1 = vector1.length;
        int [] vectorIsCapicua = new int[0];
        int nrVectorIsCapicua = 0;
        for (int i = 0; i<compVector1; i++) {
            boolean capicua = FunctionLibrary.isCapicua(vector1[i]);
            if (capicua==true) {
                nrVectorIsCapicua++;
                vectorIsCapicua=Arrays.copyOf(vectorIsCapicua,nrVectorIsCapicua);
                vectorIsCapicua[nrVectorIsCapicua-1] = vector1[i];
            }
        }
        if (nrVectorIsCapicua == 0) {
            return new int[0];
        } else
            return vectorIsCapicua;
    }

    //VIII.f

    public static int[] elemMesmoAlgarismo (int[] vector1) {
        int arrayLenght = vector1.length;
        int [] mesmoAlgarismo = new int [0];
        int nrMesmoAlgarismo = 0;
        for ( int i = 0; i<arrayLenght; i++) {
            long[] extract = extrairAlgParaArray(vector1[i]);
            boolean mesmoAlg = mesmoAlgarismo(extract);
            if (mesmoAlg == true) {
                nrMesmoAlgarismo++;
                mesmoAlgarismo = Arrays.copyOf(mesmoAlgarismo, nrMesmoAlgarismo);
                mesmoAlgarismo[nrMesmoAlgarismo - 1] = vector1[i];
            }
        }
        if (nrMesmoAlgarismo == 0) {
            return new int[]{0};
        } else
            return mesmoAlgarismo;
    }

    public static boolean mesmoAlgarismo (long [] array){
        for (int j = 0; j < array.length-1; j++) {
            if (array[j] != array[j + 1]) {
                return false;
            }
        }
        return true;
    }

    //VIII.g

    public static int [] notArmstrongVector (int [] vector1) {
        int compVector1 = vector1.length;
        int [] vectorIsNotArmstrong = new int[0];
        int nrVectorIsNotArmstrong = 0;
        for (int i = 0; i<compVector1; i++) {
            boolean isArmstrong = Exercícios4.isArmstrong(vector1[i]);
            if (isArmstrong==false) {
                nrVectorIsNotArmstrong++;
                vectorIsNotArmstrong=Arrays.copyOf(vectorIsNotArmstrong,nrVectorIsNotArmstrong);
                vectorIsNotArmstrong[nrVectorIsNotArmstrong-1] = vector1[i];
            }
        }
        if (nrVectorIsNotArmstrong == 0) {
            return new int[]{0};
        } else
            return vectorIsNotArmstrong;
    }

    //VIII.h

    public static int[] elemSeqCrescAlgN (int nrAlg, int[] arrayCrescent) {
        if (nrAlg < 2) {
            return new int [] {0};
        }
        int arrayLenght = arrayCrescent.length;
        int [] SeqCresc = new int [0];
        int nrSeqCresc = 0;
        for ( int i = 0; i<arrayLenght; i++) {
            int countAlgarismos = FunctionLibrary.numAlgarismos(arrayCrescent[i]);
            if (countAlgarismos >= nrAlg) {
                long[] extract = extrairAlgParaArray(arrayCrescent[i]);
                boolean x = isCrescent2(nrAlg,extract);
                if (x==true) {
                    nrSeqCresc++;
                    SeqCresc = Arrays.copyOf(SeqCresc, nrSeqCresc);
                    SeqCresc[nrSeqCresc - 1] = arrayCrescent[i];
                }
            }
        }
        if (nrSeqCresc == 0) {
            return new int[]{0};
        } else
            return SeqCresc;
    }

    public static boolean isCrescent2 (int nrAlg, long [] array){
        int countCrescentes = 0;
        for (int j = 0; j < array.length-1; j++) {
            if (array[j] < array[j + 1]) {
                countCrescentes++;
            }
        }
        if (countCrescentes >= nrAlg-1)
            return true;
        else return false;
    }
}