package pt.ipp.isep;

public class FunctionLibrary {

    public static int numAlgarismos(long num){
        if(num == 0)
            return 1;
        else {
            return 1+(int)(Math.log10(Math.abs(num)));
        }
    }

    public static int numAlgarismosPares(long num) {
        int nAlgPares = 0;
        do {
            if ((num % 10) % 2 == 0)
                nAlgPares++;
            num = num / 10;
        } while (num != 0);
        return nAlgPares;
    }

    public static long simetrico(long num){
        long numSimetrico = 0;

        do {
            numSimetrico = numSimetrico*10 + num % 10;
            num = num /10;
        } while(num != 0);
        return numSimetrico;
    }

    public static boolean isCapicua(long num) {
        if(simetrico(num) == num)
            return true;
        else
            return false;
    }

    public static boolean isAmstrong(long num){
        long numAmstrong = 0, aux = num;
        long alg;
        do {
            alg = aux % 10;
            numAmstrong = numAmstrong + alg*alg*alg;
            aux = aux /10;
        } while(aux != 0);
        return numAmstrong == num;
    }

    public static int[] getSequenciasCrescentes(int[] vec){

        if(vec.length <= 1)
            return new int[0];

        int[] vecAux = new int[vec.length];
        int pos = 0;
        boolean seq = false;

        for (int i = 1; i < vec.length; i++) {
            if(vec[i]>vec[i-1] && seq) {
                vecAux[pos] = vec[i];
                pos++;
            } else if(vec[i]>vec[i-1] && !seq) {
                seq = true;
                vecAux[pos] = vec[i-1];
                pos++;
                vecAux[pos] = vec[i];
                pos++;
            } else
                seq = false;
        }
        if(pos>0)
            return copyNFirstElementArray(vecAux, pos);
        else
            return new int[0];
    }

    public static int[] copyNFirstElementArray(int[] array, int n){
        int [] newArray = new int[n];

        for (int i = 0; i < n; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

    // Retorne as sequências crescentes de pelo menos n elementos de um vetor de números inteiros.
    // Um elemento não pode pertencer a mais do que uma sequência.

    public static int[] getSequenciasCrescentesN(int[] vec, int n){

        if(vec.length <= 1 || n < 2 || n > vec.length)
            return new int[0];

        int[] vecAux = new int[vec.length];
        int[] vecAuxSeq = new int[vec.length];
        int pos = 0;
        int nElemSeq = 0;
        boolean seq = false;

        for (int i = 1; i < vec.length; i++) {
            if(vec[i]>vec[i-1] && seq) {
                vecAuxSeq[nElemSeq] = vec[i];
                nElemSeq++;
            } else if(vec[i]>vec[i-1] && !seq) {
                seq = true;
                vecAuxSeq[nElemSeq] = vec[i-1];
                nElemSeq++;
                vecAuxSeq[nElemSeq] = vec[i];
                nElemSeq++;
            } else {
                if(seq && nElemSeq >= n){
                    for (int j = 0; j < nElemSeq; j++) {
                        vecAux[pos] = vecAuxSeq[j];
                        pos++;
                    }
                }
                seq = false;
                nElemSeq = 0;
            }
        }
        if(pos>0)
            return copyNFirstElementArray(vecAux, pos);
        else
            return new int[0];
    }

    //Retorne um vetor sem as sequências decrescentes em sentido lato (<=) de elementos de um vetor de números inteiros
    // passado como parâmetro.

    public static int[] elemSequenciasDecrescentesSL(int[] vec){

        if(vec.length <= 1) {
            return new int[0];
        }

        int[] vecAux = new int[vec.length];
        int pos = 1;
        boolean seq = false;

        vecAux[0]=vec[0];

        for (int i = 1; i < vec.length; i++) {
            if(vec[i]<=vec[i-1] && seq) {
            }
            else if(vec[i]<=vec[i-1] && !seq) {
                seq = true;
                pos--;
            }
            else {
                seq = false;
                vecAux[pos] = vec[i];
                pos++;
            }
        }
        if(pos>0)
            return copyNFirstElementArray(vecAux, pos);
        else
            return new int[0];
    }
}
