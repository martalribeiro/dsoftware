package pt.ipp.isep;

import java.util.Arrays;

public class Exercícios9 {

    //IX – Tendo sempre em consideração os métodos/classes desenvolvidos anteriormente, no sentido de promover a
    // reutilização de código, pretende-se que elaborem para cada alínea um método que:
    // a.Retorne as sequências crescentes de elementos de um vetor de números inteiros. Um elemento não pode pertencer a mais do que uma sequência.
    // b.Retorne as sequências crescentes de pelo menos n elementos de um vetor de números inteiros. Um elemento não pode pertencer a mais do que uma sequência.
    // c.Retorne a maior sequência crescente de elementos de um vetor de números inteiros.
    // d.Retorne a maior sequência crescente de elementos de um vetor de números de vírgula flutuante, em que a resolução de comparação é delta (argumento do método).
    // e.Retorne um vetor sem as sequências decrescentes em sentido lato (<=) de elementos de um vetor de números inteiros passado como parâmetro.
    // f.Retorne um vetor sem as sequências decrescentes em sentido lato (<=) de pelo menos n elementos de um vetor de números inteiros passado como parâmetro.
    // g.Retorne um vetor sem a maior sequência crescente em sentido lato (>=) de elementos de um vetor de números de vírgula flutuante passado como parâmetro, em que a resolução de comparação é delta (argumento do método).
    // h.Retorne um vetor sem a menor sequência crescente em sentido lato (>=) de pelo menos n elementos de um vetor de números de vírgula flutuante passado como parâmetro, em que a resolução de comparação é delta (argumento do método).


    // IX.a

    public int [] sequenciaCrescentesElementosInt (int [] vector1) {
        if(vector1.length <= 1)
            return new int[0];
        int tamanhoVector = vector1.length;
        int [] sequencia = new int[0];
        int nrSequencia = 0;
        for (int i = 0; i < tamanhoVector-1; i++) {
            if (vector1[i] < vector1[i + 1]) { //se for crescente
                nrSequencia++;
                sequencia = aumentarNrElementosVectorInt(sequencia, vector1, i, nrSequencia);
                if ( i == tamanhoVector-2) {// e estivermos perante o ultimo elemento do vector
                    nrSequencia++;
                    sequencia = aumentarNrElementosVectorInt(sequencia, vector1, i+1, nrSequencia);
                    return sequencia;
                }
            }
            else { //se for decrescente
                if (vector1[i-1] < vector1[i]) {//e for seguido de uma sequencia crescente
                    nrSequencia++;
                    sequencia = aumentarNrElementosVectorInt(sequencia, vector1, i, nrSequencia);
                }
            }
        }
        return sequencia;
    }

    public int [] aumentarNrElementosVectorInt (int [] sequencia, int [] vector1, int i, int nrVector1) {
        //aumenta o nr de espaços vazios do vector e insere o próximo elemento desejado
        sequencia = Arrays.copyOf(sequencia, nrVector1);
        sequencia[nrVector1-1] = vector1[i];
        return sequencia;
    }

    // IX.b

    public int [] sequenciaCrescentesIntNrElementos (int [] vector1, int nrElem) {
        if(vector1.length <= 1)
            return new int[0];
        if (nrElem< 2) {
            int [] res = new int [1];
            res = vector1;
            return res;
        }
        int tamanhoVector = vector1.length;
        int [] vectorFinal = new int [0];
        int [] sequenciaTeste = new int[0];
        int nrSequenciaTeste = 0;
        int nrElemInicial = 0;
        for (int i = 0; i < tamanhoVector-1; i++) {
            if (vector1[i] < vector1[i + 1]) {//se for crescente
                nrSequenciaTeste++;
                sequenciaTeste = aumentarNrElementosVectorInt(sequenciaTeste, vector1, i, nrSequenciaTeste);
                //e se o vector1 estiver no ultimo elemento e o nr de elementos preenche os requisitos
                if (i == tamanhoVector - 2 && nrSequenciaTeste >= nrElem - 1) {
                    vectorFinal = concatenarVetoresInt(vector1, i+1, vectorFinal, sequenciaTeste, nrSequenciaTeste, nrElemInicial);
                    return vectorFinal;
                }
            } else {
                if (nrSequenciaTeste >= nrElem - 1) {//e o nr de elementos preenche os requisitos
                    vectorFinal = concatenarVetoresInt(vector1, i, vectorFinal, sequenciaTeste, nrSequenciaTeste, nrElemInicial);
                    nrElemInicial = nrElemInicial + nrSequenciaTeste+1;
                }
                //reset da sequencia e do nr de elementos
                sequenciaTeste = new int[0];
                nrSequenciaTeste = 0;
            }
        }
        return vectorFinal;
    }

    public int [] concatenarVetoresInt (int [] vector1, int i, int [] vectorFinal, int [] sequenciaTeste, int nrSequenciaTeste, int nrElemInicial) {
        //metodo para concatenar dois vectores
        nrSequenciaTeste++;
        sequenciaTeste = aumentarNrElementosVectorInt(sequenciaTeste, vector1, i, nrSequenciaTeste);
        vectorFinal = Arrays.copyOf(vectorFinal,vectorFinal.length + nrSequenciaTeste);
        System.arraycopy(sequenciaTeste, 0, vectorFinal, nrElemInicial, nrSequenciaTeste);
        return vectorFinal;
    }

    // IX.c

    public int [] maiorSequenciaDeElementosInt (int [] vector1) {
        if(vector1.length <= 1)
            return new int[0];
        int tamanhoVector = vector1.length;
        int [] maior = new int [0];
        int nrMaior = 0;
        int [] sequenciaTeste = new int[0];
        int nrSequenciaTeste = 0;
        for (int i = 0; i < tamanhoVector-1; i++) {
            if (vector1[i] < vector1[i + 1]) {//se for crescente
                nrSequenciaTeste++;
                sequenciaTeste = aumentarNrElementosVectorInt(sequenciaTeste, vector1, i, nrSequenciaTeste);
                //e se o vector1 estiver no ultimo elemento e o nr de elementos crescentes é maior que a maior sequencia crescente gravada
                if ( i == tamanhoVector-2 && nrSequenciaTeste > nrMaior) {
                    nrSequenciaTeste++;
                    sequenciaTeste = aumentarNrElementosVectorInt(sequenciaTeste, vector1, i+1, nrSequenciaTeste);
                    return sequenciaTeste;
                }
            }
            else {//se for descrescente
                if (nrSequenciaTeste > nrMaior) {//e o nr de elementos crescentes é maior que a maior sequencia crescente gravada
                    maior = acumularMaiorSequenciaInt(vector1, i, sequenciaTeste, nrSequenciaTeste);
                    nrMaior = nrSequenciaTeste;
                }
                //reset da sequencia e do nr de elementos
                sequenciaTeste = new int[0];
                nrSequenciaTeste = 0;
            }
        }
        return maior;
    }

    public int [] acumularMaiorSequenciaInt (int [] vector1, int i, int [] sequenciaTeste, int nrSequenciaTeste) {
        //metodo para guardar a maior sequencia na variável maior
        nrSequenciaTeste++;
        sequenciaTeste = aumentarNrElementosVectorInt(sequenciaTeste, vector1, i, nrSequenciaTeste);
        return sequenciaTeste;
    }


    // IX.d

    public float [] maiorSequenciaDeElementos (float [] vector1, float delta) {
        if(vector1.length <= 1)
            return new float[0];
        int tamanhoVector = vector1.length;
        float [] maior = new float [0];
        int nrMaior = 0;
        float [] sequenciaTeste = new float[0];
        int nrSequenciaTeste = 0;
        //Ciclo para percorrer o vector e encontrar as sequencias crescentes e ir acumulando no novo vector
        for (int i = 0; i < tamanhoVector-1; i++) {
            if (vector1[i] < vector1[i + 1] && (Math.abs(vector1[i] - vector1[i + 1])) > delta) {
                nrSequenciaTeste++;
                sequenciaTeste = aumentarNrElementosVector(sequenciaTeste, vector1, i, nrSequenciaTeste);
                //se a sequencia estiver no fim, precisa desta condição para encaixar os ultimos valores
                if ( i == tamanhoVector-2 && nrSequenciaTeste > nrMaior) {
                    nrSequenciaTeste++;
                    sequenciaTeste = aumentarNrElementosVector(sequenciaTeste, vector1, i+1, nrSequenciaTeste);
                    return sequenciaTeste;
                }
            }
            else {
                //se o contador da sequencia atual for maior do que o guardado, atualiza a sequencia guardada
                if (nrSequenciaTeste > nrMaior) {
                    maior = acumularMaiorSequenciaFloat(vector1, i, sequenciaTeste, nrSequenciaTeste);
                    nrMaior = nrSequenciaTeste;
                }
                sequenciaTeste = new float[0];
                nrSequenciaTeste = 0;
            }
        }
        return maior;
    }

    public float [] aumentarNrElementosVector (float [] sequencia, float [] vector2, int i, int nrVector1) {
        //realizar uma copia do vector para aumentar o nr de elementos
        sequencia = Arrays.copyOf(sequencia, nrVector1);
        sequencia[nrVector1-1] = vector2[i];
        return sequencia;
    }

    public float [] acumularMaiorSequenciaFloat (float [] vector1, int i, float [] sequenciaTeste, int nrSequenciaTeste) {
        nrSequenciaTeste++;
        sequenciaTeste = aumentarNrElementosVector (sequenciaTeste, vector1, i, nrSequenciaTeste);
        return sequenciaTeste;
    }

    //IX.e

    public int [] semSequenciasDecrescentes (int [] vector1) {
        if(vector1.length <= 1)
            return new int[0];
        int compVector1 = vector1.length;
        int [] sequencia = new int [0];
        int nrSequencia = 0;
        for (int i = 0; i < compVector1-1; i++) {
            if (vector1[i] >= vector1[i+1]) {//se for decrescente
            }
            //se for crescente e estamos perante o primeiro elemento do vector1 ou for seguido por uma sequencia decrescente
            else if (i<1 || vector1[i] > vector1[i-1]) {
                nrSequencia++;
                sequencia = aumentarNrElementosVectorInt(sequencia,vector1,i,nrSequencia);
                if (i == compVector1-2) {//se estamos perante o ultimo elemento do vector
                    nrSequencia++;
                    sequencia = aumentarNrElementosVectorInt(sequencia,vector1,i+1,nrSequencia);
                }
            }
        }
        return sequencia;
    }

    //IX.f

    public int [] semSequenciasDecrescentesNrElem (int [] vector1, int nrElem) {
        FunctionLibrary c = new FunctionLibrary();
        if(vector1 == null || vector1.length == 0)
            return new int[0];
        if (nrElem< 2) {
            int [] res = new int [1];
            res = vector1;
            return res;
        }
        int compVector1 = vector1.length;
        int [] vectorFinal= new int [compVector1];
        int [] sequenciaDecrescentes = new int [0];
        int nrSequencia = 0;
        int posicao = 0;
        boolean status = true;
        for (int i = 0; i < compVector1-1; i++) {
            //se for crescente e for seguido de uma sequencia crescente
            if (vector1[i] < vector1[i+1] && status == true) {
                vectorFinal[posicao] = vector1[i];
                posicao++;
            }
            else if (vector1[i] < vector1[i+1]) {//se é crescente e é seguido de uma sequencia decrescente
                status = true;
                nrSequencia++;
                sequenciaDecrescentes = aumentarNrElementosVectorInt(sequenciaDecrescentes, vector1, i, nrSequencia);
                if (nrSequencia < nrElem) {//e a sequencia de decrescentes não estiver incluida no nr de elementos a excluir
                    System.arraycopy(sequenciaDecrescentes, 0, vectorFinal, posicao, nrSequencia);
                    posicao += nrSequencia;
                }
                //fazer reset à sequencia
                nrSequencia = 0;
                sequenciaDecrescentes = new int [0];
            }
            else {// se for decrescente
                status = false;
                nrSequencia++;
                sequenciaDecrescentes = aumentarNrElementosVectorInt(sequenciaDecrescentes, vector1, i, nrSequencia);
            }
            if ( i == compVector1-2) {// para inserir o ultimo valor do vector1 no vectorFinal
                if (vector1[i] < vector1[i+1]) {//caso seja uma sequencia crescente
                    vectorFinal[posicao] = vector1[i + 1];
                    posicao++;
                }
                else if (nrSequencia < nrElem) {//caso seja uma sequencia decrescente e o nr de elementos preenche os requisitos
                    nrSequencia++;
                    sequenciaDecrescentes = aumentarNrElementosVectorInt(sequenciaDecrescentes, vector1, i+1, nrSequencia);
                    System.arraycopy(sequenciaDecrescentes, 0, vectorFinal, posicao, nrSequencia);
                    posicao += nrSequencia;
                }
            }
        }
        //se existirem sequencias decrescentes a retirar, retira os espaços vazios não necessários do vectorFinal
        if (posicao>0) {
            vectorFinal = c.copyNFirstElementArray(vectorFinal,posicao);
            return vectorFinal;
        }
        else //se não existiram sequencias decrescentes a retirar
            return vector1;

    }

    //IX.g

    public float [] semMaiorSequenciasCrescente (float [] vector1, float delta) {
        if(vector1.length <= 1)
            return new float[0];
        int compVector1 = vector1.length;
        int nrCrescentes = 0;
        int nrVectorFinal = 0;
        int nrMaior = 0;
        int primeiraPosicao = 0;
        int ultimaPosicao = 0;

        for (int i = 0; i < compVector1-1; i++) {
            //se for uma sequencia crescente e a diferença entre os elementos está dentro do delta
            if (vector1[i] < vector1[i+1] || Math.abs(vector1[i]-vector1[i+1]) <= Math.abs(delta)) {
                nrCrescentes++;
            }
            else {//se for uma sequencia decrescente
                if (nrCrescentes>nrMaior) {//e o nr de elementos crescentes é maior que a maior sequencia crescente gravada
                    nrMaior=nrCrescentes;
                    //grava o indice do inicio e do fim da maior sequencia crescente
                    primeiraPosicao = i-nrCrescentes;
                    ultimaPosicao = i+1;
                }
                nrCrescentes=0;//reset ao contador da ultima sequencia
            }
        }

        //caso a sequencia maior esteja no final do vector1, atualiza as posições de inicio e fim da maior sequencia
        if (nrCrescentes>nrMaior) {
            primeiraPosicao = vector1.length-1-nrCrescentes;
            ultimaPosicao = vector1.length;
        }

        //se não exister uma sequencia crescente, retorna o vector dado
        if (primeiraPosicao == 0 && ultimaPosicao == 0) {
            return new float [0];
        }

        float [] vectorFinal = criarVectorFinal(vector1,primeiraPosicao,ultimaPosicao,nrVectorFinal);
        return vectorFinal;
    }

    //metodo que corre dois ciclos para preencher um vector sem terminada elementos adjacentes
    public float [] criarVectorFinal (float [] vector1, int primeiraPosicao, int ultimaPosicao, int nrVectorFinal) {
        float [] vectorFinal = new float[0];
        for (int i = 0; i < primeiraPosicao; i++) {
            nrVectorFinal++;
            vectorFinal = Arrays.copyOf(vectorFinal, nrVectorFinal);
            vectorFinal[nrVectorFinal-1] = vector1[i];
        }
        for (int j = ultimaPosicao; j < vector1.length; j++) {
            nrVectorFinal++;
            vectorFinal = Arrays.copyOf(vectorFinal, nrVectorFinal);
            vectorFinal[nrVectorFinal-1] = vector1[j];
        }
        return vectorFinal;
    }

    //IX.h

    public float [] semMenorSequenciasCrescenteNElem (float [] vector1, float delta, int nrElem) {
        if(vector1.length <= 1)
            return new float[0];
        if (nrElem < 2) {
            float [] res = new float [1];
            res = vector1;
            return res;
        }
        int compVector1 = vector1.length;
        int nrCrescentes = 0;
        int nrVectorFinal = 0;
        int nrMenor = vector1.length;
        int primeiraPosicao = 0;
        int ultimaPosicao = 0;
        for (int i = 0; i < compVector1-1; i++) {
            //se for uma sequencia crescente e a diferença entre os elementos está dentro do delta
            if (vector1[i] < vector1[i+1] || Math.abs(vector1[i]-vector1[i+1]) <= Math.abs(delta)) {
                nrCrescentes++;
            }
            else {//se for uma sequencia descrescente
                if (nrCrescentes<nrMenor && nrCrescentes >= nrElem-1) {//e o nr de elementos crescentes é menor que a menor sequencia crescente gravada
                    nrMenor=nrCrescentes;
                    primeiraPosicao = i-nrCrescentes;
                    ultimaPosicao = i+1;
                }
                //reset ao contador de elementos
                nrCrescentes=0;
            }
        }
        //caso a sequencia menor esteja no final do vector1, atualiza as posições de inicio e fim da menor sequencia
        if (nrCrescentes<nrMenor && nrCrescentes >= nrElem-1) {
            primeiraPosicao = vector1.length-1-nrCrescentes;
            ultimaPosicao = vector1.length;
        }
        //se não exister uma sequencia crescente, retorna o vector dado
        if (primeiraPosicao == 0 && ultimaPosicao == 0) {
            return vector1;
        }
        float [] vectorFinal = criarVectorFinal(vector1,primeiraPosicao,ultimaPosicao,nrVectorFinal);
        return vectorFinal;
    }
}
