package pt.ipp.isep;

/*Segue o 3º grupo de exercícios:
III. Para cada uma das alíneas seguintes elabore um método que:
a. retorne o número de algarismos de um número inteiro longo: int numAlgarismos( long num)
b. retorne o número de algarismos pares de um número inteiro longo
c. retorne o número de algarismos ímpares de um número inteiro longo
d. retorne a média dos algarismos pares de um número inteiro longo
e. receba um número inteiro longo e retorne um outro que corresponde ao original sem os algarismos pares.
 */

public class Exercícios3 {
    //III.a

    public long numAlgarismos(long num) {
        int i = 1;
        if (num < 0) {
            num = num * (-1);// tornar os números negativos positivos
        }
        while (num >= 10) {
            num = num / 10;//ao dividir por 10 conseguimos baixar das centenas para as dezenas e das dezenas para as
            i++;            // unidades e etc em cada ciclo
        }
        return i;
    }

    ///III.b
    public long numAlgarismosPares(long num) {
        int i = 0;
        if (num < 0) {
            num = num * (-1);
        }
        while (num>=10) {
            if (num % 2 == 0) {//definir os números pares
                i++;
            }
            num = num / 10;
        }
        if (num>=0 && num<10) {
            if (num % 2 == 0) {
                i++;
            }
            else return i;
        }
        return i;
    }

    ///III.c
    public long numAlgarismosImpares(long num) {
        return numAlgarismos(num) - numAlgarismosPares(num);
    }

    //III.d
    public double mediaAlgarismosPares (long num) {
        int i = 0;
        long soma = 0;
        if (num < 0) {
            num = num * (-1);
        }
        while (num>=10) {
            if (num % 2 == 0) {//define os pares
                i++;
                long x = num%10;// o resto da divisão por 10 dá-nos sempre o último algarismo do número
                soma = soma + x;// assim vai somar todos os algarismos pares definidos em cima
            }
            num = num / 10;
        }
        if (num>=0 && num<10) {
            if (num % 2 == 0) {
                i++;
                soma = soma + num;//como só resta o ultimo número, soma essa número à soma em cima
            }
        }
        return (double)soma/i;//média corresponde à soma a dividir pelo número de ciclos que foi feito (igual ao nr de
    }                           // algarismos
    //III.e
    public long retornarNumero (long num) {
        long alg = 0; //algarismo impar que é gerado em cada ciclo e que queremos armazenar
        int i = 0; //número de ciclos vai nos indicar a posição do número no algarismo pois indica o nr de vezes que se dividiu por 10
        double x = 0; // variável em que armazenamos todos os números impares que queremos retornar
        if (num < 0) {
            num = num * (-1);
        }
        while (num >= 10) {
            if (num % 2 != 0) {//gera os numeros impares
                alg = num % 10;//resto da divisão por 10 gera o ultimo algarismo do número
                x = x + alg*Math.pow(10,i);//função que vai pegar no número que foi gerado e lhe vai alocar uma posição
                i++;                           // essa posição matematicamente define-se por uma potência de base 10 com
                // expoente i (se dividimos por 10 i vezes, também temos de multiplicar
            }                                   // por 10 o mesmo número de vezes, o número 3 se era de 300 tem de ser
            num = num / 10;                     //multiplicado por 100
        }
        if (num >= 0 && num < 10) {
            if (num % 2 != 0) {
                alg = num;
                x = x+alg*Math.pow(10,i);
                i++;
            }

        }
        return (long) x; //retorna a caixinha que estivemos a colectar antes!
    }
}

