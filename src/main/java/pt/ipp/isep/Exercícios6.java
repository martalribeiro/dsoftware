package pt.ipp.isep;

/*
VI. Para cada uma das alíneas seguintes elabore um método que retorne:
a) o número de elementos num vector
b) o maior elemento de um vector
c) o menor elemento de um vector
d) o número de elementos não nulos
e) a média de todos os elementos
f) a média de todos os números pares
g) a média de todos os múltiplos de um número passado como argumento
h) True caso o vector esteja vazio, False em caso contrário
i) True caso o vector contenha apenas um elemento, False em caso contrário
j) True se o vector tiver apenas elementos pares, False em caso contrário
k) True se o vector tiver elementos duplicados, False em caso contrário (assuma vetor ordenado por ordem crescente)
 */


import java.util.Random;

public class Exercícios6 {

    //VI.a
    public static int retornarElementosVector(int[] vector) {
        return vector.length;
    }

    //VI.b
    public static int retornarMaximoVector(int[] vector) {
        int l1 = vector[0];
        int l2 = vector.length;
        int ind = 0;
        for (int i = 0; i<l2; i++) {
            if (vector[ind]>l1) {
                l1 = vector[ind];
            }
            ind++;
        }
        return l1;
    }

    //VI.c
    public static int retornarMinimoVector(int[] vector) {
        int l1 = vector[0];
        int l2 = vector.length;
        int ind = 0;
        for (int i = 0; i<l2; i++) {
            if (vector[ind]<l1) {
                l1=vector[ind];
            }
            ind++;
        }
        return l1;
    }

    //VI.d
    public static int retornarElemNNulos(int[] vector) {
        int l1 = vector[0];
        int l2 = vector.length;
        int conta = 0;
        int ind = 0;
        for (int i=0; i<l2; i++) {
            l1=vector[ind];
            if (l1!=0) {
                conta++;
            }
            ind++;
        }
        return conta;
    }

    //VI.e
    public static double medElements(int[] vector) {
        int l2 = vector.length;
        int soma = 0;
        for (int i=0; i<l2;) {
            int l1 = vector[i];
            soma = soma + l1;
            i++;
        }
        double average = (double)soma/l2;
        double result = (double) Math.round(average*100)/100;
        return result;
    }

    //VI.f
    public static double medPares(int[] vector) {
        int l2 = vector.length;
        int soma = 0;
        int conta = 0;
        for (int i=0; i<l2;) {
            int l1=vector[i];
            i++;
            if (l1%2==0) {
                conta++;
                soma = soma + l1;
            }
        }
        double average = (double)soma/conta;
        double result = (double) Math.round(average*100)/100;
        return result;
    }

    //VI.g
    public static double medMultArg(int[] vector, int n) {
        if (n==0) {
            return -1;
        }
        int l2 = vector.length;
        int soma = 0;
        int conta = 0;
        for (int i = 0; i<l2;) {
            int l1 = vector[i];
            i++;
            if (l1%n==0) {
                soma = soma + l1;
                conta++;
            }
        }
        double average = (double) soma/conta;
        double result = (double) Math.round(average*100)/100;
        return result;
    }

    //VI.h
    public static boolean vectVazio(int[] vector) {
        int a = vector.length;
        if (a == 0) {
            return true;
        }
        else return false;
    }

    //VI.i
    public static boolean vectUm(int[] vector) {
        int a = vector.length;
        if (a == 1) {
            return true;
        }
        else return false;
    }

    //VI.j
    public static boolean vectPares(int[] vector) {
        int l2 = vector.length;
        int l1 = vector[0];
        int ind = 0;
        for (int i = 0; i<l2; i++) {
            if (l1 % 2 == 0) {
                l1=vector[ind];
                ind++;
            }
        }
        if (l1%2==0) {
            return true;
        }
        else return false;
    }

    //VII.k
    public static boolean vectDupl(int[] vector) {
        int comp = vector.length;
        for (int i = 1; i<comp;i++) {
            if (vector[i-1] == vector[i]) {
                return true;
            }
        }
        return false;
    }

    //método para gerar os vectores random:

    public int[] gerarArrays () {
        int vector[] = new int[10];
        int comprimento = vector.length;
        for (int i = 0; i<comprimento; i++) {
            Random r = new Random(0);
            vector[i]= r.nextInt(10); //bound é o limite final do intervalo, este vai de 1 a 10
        }
        vector[0]=100;
        return vector;
    }
}