package objectsTraining;

import pt.ipp.isep.Exercícios6;
import pt.ipp.isep.Exercícios8;
import pt.ipp.isep.FunctionLibrary;

import java.util.Arrays;

/**
 * X. Desenvolva uma classe Array (ou Vetor, se preferir) que encapsule um vetor de inteiros (i.e. int vetor[]) através dos seguintes métodos:
 * @author marta
 */
public class Array {

    private int [] myVector;

    /**
     * X.1 Construtor público com um parâmetro vetor, que inicializa os dados do vetor encapsulado.
     * @param vector1 cannot be null or empty
     */

    public Array (int [] vector1) {
        if( vector1 != null) {
            myVector = new int[vector1.length];
            for (int i = 0; i < myVector.length; i++) {
                this.myVector[i] = vector1[i];
            }
        }
    }

    /**
     *
     * @param o
     * @return true if two objects are equal in value, else false
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Array vector1 = (Array) o;
        //faz com que sempre que comparamos Arrays, compara o valor interno do array
        return Arrays.equals(myVector, vector1.myVector);

        //faz a mesma coisa mas de forma muito mais elaborada, sem usar um objecto da classe Arrays
        /*if (myVector.length != vector1.myVector.length) {
            return false;
        }
        for (int i = 0; i < vector1.myVector.length; i++) {
            if (myVector[i] != vector1.myVector[i]) {
                return false;
            }
        }
        return true;*/
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(myVector);
    }

    /**
     * X.2
     * @return número de elementos no vetor.
     */

    public int nrElemVector () {
        return myVector.length;
    }

    /**
     * X.3
     * @return o maior elemento do vetor.
     */

    public int majorElemVector () {
        return Exercícios6.retornarMaximoVector(myVector);
    }

    /**
     * X.4
     * @return o menor elemento do vetor.
     */

    public int minorElemVector () {
        if (myVector.length == 0) {
            return 0;
        }
        int menor = myVector[0];

        for (int i = 0; i< myVector.length; i++) {
            if (myVector[i] < menor) {
                menor = myVector[i];
            }
        }
        return menor;
    }

    /**
     * X.5
     * @return número de elementos não nulos.
     */

    public int nrElementosNNulos () {
        int conta = 0;
        for (int i=0; i< myVector.length; i++) {
            if (myVector[i] != 0) {
                conta++;
            }
        }
        return conta;
    }

    /**
     * X.6
     * @return a média de todos os elementos.
     */

    public double mediaElem () {
        int soma = 0;
        for (int i=0; i<myVector.length; i++) {
            soma = soma + myVector[i];
        }
        double average = (double)soma/myVector.length;
        return average;
    }

    /**
     * X.7
     * @return a média de todos os números pares.
     */

    public double mediaPares () {
        if (myVector.length == 0) {
            return 0;
        }
        int soma = 0;
        int conta = 0;
        for (int i=0; i< myVector.length; i++) {
            if (myVector[i]%2==0) {
                conta++;
                soma = soma + myVector[i];
            }
        }
        if (conta==0) {
            return 0;
        }
        double average = (double)soma/conta;
        return average;
    }

    /**
     * X.8
     * @param number numero do qual é multiplo
     * @return a média de todos os múltiplos de um número passado como argumento.
     */

    public double mediaMult (int number) {
        return Exercícios6.medMultArg(myVector, number);
    }

    /**
     * X.9
     * @return True caso o vetor esteja vazio, False em caso contrário.
     */

    public boolean vectorVazio () {
        return Exercícios6.vectVazio(myVector);
    }

    /**
     * X.10
     * @return True caso o vetor contenha apenas um elemento, False em caso contrário.
     */

    public boolean vectorUmElem () {
        return Exercícios6.vectUm(myVector);
    }

    /**
     * X.11
     * @return True se o vetor tiver apenas elementos pares, False em caso contrário.
     */

    public boolean vectorElemPares () {
        return Exercícios6.vectPares(myVector);
    }

    /**
     * X.12
     * @return True se o vetor tiver elementos duplicados, False em caso contrário (assuma vetor ordenado por ordem crescente).
     */

    public boolean vectorDuplicados () {
        return Exercícios6.vectDupl(myVector);
    }

    /**
     * X.13
     * @return os elementos do vetor cujo número de algarismos é superior número médio de algarismos de todos os elementos do vetor.
     */

    public int somaAlgarismosVector () {
        int soma = 0;
        for (int i = 0; i < myVector.length; i++) {
            int contaAlgarismos = FunctionLibrary.numAlgarismos(myVector[i]);
            soma = soma + contaAlgarismos;
        }
        return soma;
    }

    public double mediaAlgarismosVector() {
        int somaAlgarismos = somaAlgarismosVector();
        int nrNumerosArray = myVector.length;
        double media = (double) somaAlgarismos/nrNumerosArray;
        return media;
    }

    public int [] mediaSuperiorMediaAlgarismos () {
        if(myVector.length <= 1)
            return new int[0];
        double media = mediaAlgarismosVector();
        int [] vectorAlgSupMedia = new int [myVector.length];
        int countVectorAlgSupMedia = 0;
        int contaAlg;
        for (int i = 0; i< myVector.length; i++) {
            contaAlg = FunctionLibrary.numAlgarismos(myVector[i]);
            if (contaAlg > media) {
                vectorAlgSupMedia [countVectorAlgSupMedia] = myVector[i];
                countVectorAlgSupMedia++;
            }
        }
        if (vectorAlgSupMedia.length < 0) {
            return new int[0];
        }
        else {
            vectorAlgSupMedia = FunctionLibrary.copyNFirstElementArray(vectorAlgSupMedia, countVectorAlgSupMedia);
            return vectorAlgSupMedia;
        }
    }

    /**
     * X.14
     * @return os elementos do vetor cuja percentagem de algarismos pares é superior à média da percentagem de algarismos pares de todos os elementos do vetor.
     */

    public double percentagemPares (long num) {
        long algPares = FunctionLibrary.numAlgarismosPares(num);
        long countAlgarismos = FunctionLibrary.numAlgarismos(num);
        double percPares = (double) algPares/countAlgarismos;
        return percPares;
    }

    public double mediaPercentagemPares () {
        double somaPercPares = 0;
        int contaNum = 0;
        for (int i = 0; i< myVector.length; i++) {
            double percPares = percentagemPares(myVector[i]);
            contaNum++;
            somaPercPares = somaPercPares + percPares;
        }
        if (contaNum == 0) {
            return 0;
        }
        double mediaPercentPares = somaPercPares/contaNum;
        return mediaPercentPares;
    }

    public int [] percentParesSuperiorMedia () {
        if(myVector.length == 0)
            return new int[0];
        double media = mediaPercentagemPares();
        int [] vectorAlgSupMedia = new int [myVector.length];
        int pos = 0;
        for (int i = 0; i< myVector.length; i++) {
            if (percentagemPares(myVector[i]) > media) {
                vectorAlgSupMedia[pos] = myVector[i];
                pos++;
            }
        }
        if (pos < 0) {
            return new int[0];
        }
        else {
            vectorAlgSupMedia = FunctionLibrary.copyNFirstElementArray(vectorAlgSupMedia, pos);
            return vectorAlgSupMedia;
        }
    }

    /**
     * X.15
     * @return os elementos compostos exclusivamente por algarismos pares do vetor.
     */

    public int [] elemExclAlgPares () {
        if(myVector.length <= 1)
            return new int[0];
        int [] algPares = new int[myVector.length];
        int nrAlgPares = 0;
        for (int i=0; i <myVector.length; i++) {
            double percPares = percentagemPares(myVector[i]);
            if (percPares == 1) {
                algPares[nrAlgPares] = myVector[i];
                nrAlgPares++;
            }
        }
        if (nrAlgPares == 0) {
            return new int[0];
        } else
            algPares = FunctionLibrary.copyNFirstElementArray(algPares, nrAlgPares);
            return algPares;
    }

    /**
     * X.16
     * @return os elementos que são sequências crescentes (e.g. 347) do vetor.
     */

    public int[] elemSeqCresc () {
        int [] SeqCresc = new int [0];
        int nrSeqCresc = 0;
        for ( int i = 0; i< myVector.length; i++) {
            if (Math.abs(myVector[i]) > 10 ) {
                long[] extract = extrairAlgParaArray(myVector[i]);
                boolean x = Exercícios8.isCrescent(extract);
                if (x == true) {
                    nrSeqCresc++;
                    SeqCresc = Arrays.copyOf(SeqCresc, nrSeqCresc);
                    SeqCresc[nrSeqCresc - 1] = myVector[i];
                }
            }
        }
        if (nrSeqCresc == 0) {
            return new int[]{0};
        } else
            return SeqCresc;
    }

    public long [] extrairAlgParaArray (long num) {
        int count = FunctionLibrary.numAlgarismos(num);
        long [] arrayAlgarismo = new long [count];
        int i = count-1;
        while ( num != 0) {
            num = Math.abs(num);
            long algarismo = num % 10;
            arrayAlgarismo[i] = algarismo;
            i--;
            num = num/10;
        }
        return arrayAlgarismo;
    }

    /**
     * X.17
     * @return as capicuas existentes no vetor.
     */

    public int [] capicuaVector () {
        return Exercícios8.capicuaVector(myVector);
    }

    /**
     * X.18
     * @return
     */

    public int [] elemMesmoAlgarismo () {
        return Exercícios8.elemMesmoAlgarismo(myVector);
    }

    /**
     * X.19
     * @return os números que não são de Amstrong existentes no vetor.
     */

    public int [] notArmstrongVector () {
        return Exercícios8.notArmstrongVector(myVector);
    }

    /**
     * X.20
     * @param nrAlg numero inteiro que define o número de digitos que compõem
     * @return os elementos que contêm uma sequência crescente de pelo menos n algarismos (e.g. n=3, 347) do vetor.
     */

    public int [] elemSeqCrescAlgN (int nrAlg) {
        return Exercícios8.elemSeqCrescAlgN(nrAlg,myVector);
    }

    /**
     * X.21
     * @param vector2
     * @return verdadeiro ou falso, consoante o vetor é igual a um vetor passado por parâmetro.
     */

    public boolean vectorIgual (int [] vector2) {
        if (vector2.length == 0) {
            return false;
        }
        if (myVector.length != vector2.length) {
            return false;
        }
        for (int i = 0; i < vector2.length; i++) {
            if (myVector[i] != vector2 [i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * X.22 Multiplique o vetor por um valor inteiro passado por parâmetro.
     * @param number numero pelo qual vai ser feita a multiplicação
     */

    public void multiplyByNumber (int number) {
        for (int i = 0; i < myVector.length; i++) {
            myVector[i] = myVector[i] * number;
        }
    }

    /**
     * X.23 Multiplique o vetor por outro Vetor passado por parâmetro.
     * @param vector1 vector pelo qual vai ser feita a multiplicação
     */

    public void multiplyByVector (int [] vector1) {
        if (myVector.length == vector1.length && vector1.length != 0) {
            for (int i = 0; i < myVector.length; i++) {
                myVector[i] = myVector[i] * vector1[i];
            }
        }
    }

    /**
     * X.24 Inverta a ordem dos valores do vetor.
     */

    public void invertOrder () {
        int [] arrayAux = new int [myVector.length];
        for (int i = myVector.length-1, j=0; i > -1; i--, j++) {
            arrayAux[j] = myVector[i];
        }
        myVector = Arrays.copyOf(arrayAux,arrayAux.length);
    }

}
