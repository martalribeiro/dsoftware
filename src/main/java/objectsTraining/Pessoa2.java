package objectsTraining;

import java.util.HashSet;
import java.util.Objects;

/**
 *XII. Desenvolva uma classe Pessoa capaz de encapsular e representar o conceito de Pessoa, disponibilizando os seguintes métodos:
 */

public class Pessoa2 {

    private String nome;
    private String dataDeNascimento;
    private HashSet<Pessoa2> irmaos;

    /**
     * XII.1 Construtor público com parâmetro nome (string) e data de nascimento (string).
     */

    public Pessoa2(String nome, String dataDeNascimento) {
        this.nome = nome;
        this.dataDeNascimento = dataDeNascimento;
        this.irmaos = new HashSet<Pessoa2>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pessoa2 pessoa = (Pessoa2) o;
        return Objects.equals(nome, pessoa.nome) &&
                Objects.equals(dataDeNascimento, pessoa.dataDeNascimento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, dataDeNascimento, irmaos);
    }

    /**
     * XII.6
     * Adicione um irmão.
     */

    public void adicionarIrmao (Pessoa2 newIrmao) {
        this.irmaos.add(newIrmao);
    }

    public HashSet<Pessoa2> getIrmaos () {
        return this.irmaos;
    }

    /**
     * XII.7
     * Retire um irmão.
     */

    public void retirarIrmao (Pessoa2 oldIrmao) {
        this.irmaos.remove(oldIrmao);
    }

    /**
     * XII.8
     * Determine se duas pessoas têm os mesmos irmãos.
     */

    public boolean mesmosIrmaos (Pessoa2 oldIrmao) {
        // Se não tiverem o mesmo número de irmaos, retorna false
        if ( this.irmaos.size() != oldIrmao.irmaos.size()) {
            return false;
        }

        //copia dos hashSets de ambos os irmaos, para não alterarmos os seus valores internos
        HashSet <Pessoa2> copie = new HashSet<Pessoa2>(this.irmaos);
        HashSet <Pessoa2> oldCopie = new HashSet<Pessoa2>(oldIrmao.irmaos);

        //remover o irmao da lista do irmao1 e vice-versa porque para terem os mesmos irmaos, tem de ser
        // obrigatoriamente irmaos
        copie.remove(oldIrmao);
        oldCopie.remove(this);

        if (! copie.contains(oldIrmao) || ! oldCopie.contains(this)) {
            return false;
        }
        else {
            return true;
        }
    }

}
