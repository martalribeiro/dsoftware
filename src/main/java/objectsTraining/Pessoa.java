package objectsTraining;

import java.util.Arrays;
import java.util.Objects;

/**
 *XII. Desenvolva uma classe Pessoa capaz de encapsular e representar o conceito de Pessoa, disponibilizando os seguintes métodos:
 */

public class Pessoa {

    private String nome;
    private String dataDeNascimento;
    private String localDeNascimento;
    private Pessoa mae;
    private Pessoa pai;
    private Pessoa [] irmaos;

    /**
     * XII.1 Construtor público com parâmetro nome (string) e data de nascimento (string).
     */

    public Pessoa (String nome, String dataDeNascimento, Pessoa mae, Pessoa pai){
        this.nome = nome;
        this.dataDeNascimento = dataDeNascimento;
        this.mae = mae;
        this.pai = pai;
        this.irmaos = new Pessoa[0];
    }

    //construtor cópia para proteção dos atributos privados
    public Pessoa (Pessoa p1) {
        this.nome = p1.nome;
        this.dataDeNascimento = p1.dataDeNascimento;
        this.mae = new Pessoa (p1.mae);
        this.pai = new Pessoa (p1.pai);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pessoa pessoa = (Pessoa) o;
        return  Objects.equals(nome, pessoa.nome) &&
                Objects.equals(dataDeNascimento, pessoa.dataDeNascimento) &&
                Objects.equals(localDeNascimento, pessoa.localDeNascimento) &&
                Objects.equals(mae, pessoa.mae) &&
                Objects.equals(pai, pessoa.pai) &&
                Arrays.equals(this.irmaos, pessoa.irmaos);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(nome, dataDeNascimento, localDeNascimento, mae, pai, nrIrmaos, nrEspacosAumentar);
        result = 31 * result + Arrays.hashCode(irmaos);
        return result;
    }

    /**
     * XII.2
     * @param nova
     * @return true se duas pessoas têm a mesma data de nascimento.
     */

    public boolean mesmaDataDeNascimento (Pessoa nova) {
        if (this.dataDeNascimento == null || nova.dataDeNascimento == null) {
            return false;
        }
        if (! this.dataDeNascimento.equals(nova.dataDeNascimento)) {
            return false;
        }
        else return true;
    }

    /**
     * XII.3
     * @param nova
     * @return Determine se duas pessoas têm o mesmo nome.
     */

    public boolean mesmoNome (Pessoa nova) {
        if (! this.nome.equals(nova.nome)) {
            return false;
        }
        return true;
    }

    /**
     * XII.4 Defina o local de nascimento.
     * @param localDeNascimento
     */

    public void setLocalDeNascimento (String localDeNascimento) {
        this.localDeNascimento = localDeNascimento;
    }

    public String getLocalDeNascimento () {
       return this.localDeNascimento;
    }

    /**
     * XII.5
     * @param nova
     * @return true se duas pessoas têm o mesmo local de nascimento
     */

    public boolean mesmoLocalNascimento (Pessoa nova) {
        if (! this.localDeNascimento.equalsIgnoreCase(nova.localDeNascimento)) {
            return false;
        }
        return true;
    }

    /**
     * XII.6
     * Adicione um irmão.
     */

    private int nrIrmaos = 0;

    public void adicionarIrmao (Pessoa newIrmao) {
        irmaos = Arrays.copyOf(irmaos, irmaos.length+1);
        irmaos[irmaos.length-1] = newIrmao;
        nrIrmaos++;
    }

    public Pessoa [] getIrmaos () {
        return this.irmaos;
    }

    ///////////////// opção 2 //////////////////////

    private int nrEspacosAumentar = 30;

    private void aumentarArrayIrmaos () {
        if (irmaos.length == nrIrmaos) {
            //expandir o array de Pessoas irmaos
            Pessoa [] aux = Arrays.copyOf(irmaos, nrIrmaos+nrEspacosAumentar);
            irmaos = aux;
        }
    }

    public void adicionarIrmao2 (Pessoa irmao1) {
        // Procurar um espaço vazio (ie com valor nulo)
        int length = irmaos.length;
        for (int i = 0; i < length; i++) {
            // Se encontrar espaço vazio -> pode adiconar lá o irmao
            if (irmaos[i] == null) {
                // Aicionar irmao
                irmaos[i] = irmao1;
                nrIrmaos++;

                // Método acaba aqui, não há necessidade de procurar mais posições vazias
                return;
            }
        }

        // Se o programa chega aqui, significa que não existem mais posições vazias
        // Precisamos então de criar mais espaços
        aumentarArrayIrmaos();

        // Adicionar novo irmão -> depois de expandir o array, length está vazio porque acabamos de expandir o Array
        irmaos[length] = irmao1;
        nrIrmaos++;
    }

    /**
     * XII.7
     * Retire um irmão.
     */

    public void retirarIrmao (Pessoa irmao1) {
        if ( nrIrmaos != 0) {
            nrIrmaos--;
            Pessoa[] aux = new Pessoa[irmaos.length];
            int pos = 0;
            for (int i = 0; i < irmaos.length; i++) {
                if (!irmaos[i].equals(irmao1)) {
                    aux[pos] = irmaos[i];
                    pos++;
                }
            }
            aux = copyPessoasArray(aux, pos);
            irmaos = aux;
        }
    }

    private Pessoa [] copyPessoasArray(Pessoa [] array, int n){
        Pessoa [] newArray = new Pessoa [n];

        for (int i = 0; i < n; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

    /////////////////// opção 2 ///////////////////////

    public void retirarIrmao2 (Pessoa irmao1) {
        if (nrIrmaos != 0) {
            for (int i = 0; i < nrIrmaos; i++) {
                if (irmaos[i].equals(irmao1)) {
                    irmaos[i] = null;    // remove o irmao
                    nrIrmaos--;
                }
            }
        }
    }


    /**
     * XII.8
     * Determine se duas pessoas têm os mesmos irmãos.
     */

    public boolean mesmosIrmaos (Pessoa irmao1) {
        // Se não tiverem o mesmo número de irmaos, retorna false
        if ( this.irmaos.length != irmao1.irmaos.length) {
            return false;
        }

        //copia da pessoa que neste momento é o objecto desta classe, para não alterarmos os seus valores internos
        Pessoa copie = new Pessoa (this);
        Pessoa copieIrmao1 = new Pessoa (irmao1);
        //remover o irmao da lista do irmao1 e vice-versa porque para terem os mesmos irmaos, tem de ser
        // obrigatoriamente irmaos
        copie.retirarIrmao(irmao1);
        copieIrmao1.retirarIrmao(copie);


        for (int i = 0; i < copie.irmaos.length; i++) {
            // Para cada irmao numa pessoa
            Pessoa p1 = copie.irmaos[i];
            boolean found = false;

            // Vamos verificar se a segunda pessoa tem o mesmo irmao que a primeira
            for (int j = 0; j < irmao1.irmaos.length; j++) {
                Pessoa p2 = irmao1.irmaos[j];
                if (!found && p1.equals(p2)) {
                    found = true;
                }
            }

            // se a variável found não for alterada, siginifica que há irmaos diferentes e retorna false
            if (!found) {
                return false;
            }
        }
        // não encontra irmaos diferentes, portanto as pessoas vão ter os mesmos irmaos
        return true;
    }

    ///////////////////// opção 2 //////////////////////////////////

    public boolean verificarSeMesmosIrmaos2 (Pessoa nova) {
        //Aqui preciso de comparar o valor interno do Array, não posso usar Array Equals
        //porque Irmaos da Pessoa 1 (PersonA, PersonB, PersonC)
        //Irmaos Pessoa 2 (PersonB, PersonA, PersonC)
        //Array.equals vai sempre dizer que a pessoa 1 e 2 não irmãos nem têm os mesmo irmaos!

        // Se não tiverem o mesmo número de irmaos, retorna false
        if (nova.irmaos.length != this.irmaos.length) {
            return false;
        }

        for (int i = 0; i < this.irmaos.length; i++) {
            // Para cada irmao numa pessoa
            Pessoa p1 = this.irmaos[i];

            // Podemos ter nulos no array por causa da expansao e por causa de retirar irmao
            if (p1 != null) {
                boolean found = false;

                // Vamos verificar se a segunda pessoa tem o mesmo irmao que a primeira
                for (int j = 0; j < nova.irmaos.length; j++) {
                    Pessoa p2 = nova.irmaos[j];
                    if (!found && p1.equals(p2)) {
                        found = true;
                    }
                }

                // se nao encontrar nada significa que não encontrou nenhum irmao em comum
                if (!found) {
                    return false;
                }
            }
        }

        return true;  // não encontra irmaos diferentes, portanto as pessoas vão ter os mesmos irmaos
    }

    /**
     * XII.9
     * Altere o construtor para receber por parâmetro a mãe e pai.
     */

    //adicionei ao construtor

    /**
     * XII.10
     * Determinar se duas pessoas são irmãos ou meio-irmãos.
     */

    public String irmaosOuMeiosIrmao (Pessoa nova) {
        if (this.mae.equals(nova.mae) && ! this.pai.equals(nova.pai) || this.pai.equals(nova.pai) && ! this.mae.equals(nova.mae)) {
            return "Sao meios-irmaos.";
        }
        if (this.mae.equals(nova.mae) && this.pai.equals(nova.pai)) {
            return "Sao irmaos.";
        }
        else {
            return "Nao sao irmaos.";
        }
    }


    /**
     * XII.11
     * Determinar se duas Pessoa representam a mesma pessoa: se tiverem os mesmos pais, o mesmo nome, o mesmo local de
     * nascimento e a mesma data de nascimento.
     */

    //fiz através do método equals

    //segunda opção:

    public boolean mesmaPessoa (Pessoa nova) {
        if (! this.mae.equals(nova.mae) || ! (this.pai.equals(nova.pai)) || ! this.nome.equals(nova.nome) ||
               ! this.localDeNascimento.equals(nova.localDeNascimento) || ! this.dataDeNascimento.equals(nova.dataDeNascimento)) {
            return false;
        }
        else return true;
    }
}


