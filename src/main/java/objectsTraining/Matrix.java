package objectsTraining;

import pt.ipp.isep.FunctionLibrary;

import javax.swing.event.CellEditorListener;
import java.util.Arrays;

/**
 *XI. Desenvolva uma classe Matriz que encapsule um vetor bidimensional de inteiros (i.e. int vetorBidimensional[][]) através dos seguintes métodos:
 */

public class Matrix {

    private int[][] cells;

    /**
     * XI.1 Construtor público com um parâmetro vetor bidimensional, que inicializa os dados do vetor bidimensional encapsulado.
     * @param newMatrix
     */

    public Matrix(int[][] newMatrix) {
        this.cells = newMatrix;
    }

    /**
     * Método Equals
     * @param o
     * @return true if two matrices are equal, false if they are not
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Matrix matrix1 = (Matrix) o;
        //faz com que sempre que comparamos Matrizes, compara o valor interno e profundo da matriz
        return Arrays.deepEquals(cells, matrix1.cells);

        //faz a mesma coisa mas de forma muito mais elaborada, sem usar um objecto da classe Arrays
        /*if (cells.length != matrix1.cells.length) {
            return false;
        }
        for (int i = 0; i< matrix1.cells.length; i++) {
            for (int j = 0; j < matrix1.cells[0].length; j++) {
                if (cells[i][j] != matrix1.cells[i][j]) {
                    return false;
                }
            }
        }
        return true;*/
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.cells);
    }

    /**
     * @return first element of the matrix
     */

    public int getFirstElement() {
        return this.cells[0][0];
    }

    /**
     * Method to fill the matrix
     * @param matrix1
     */

    public void fillCells (int [][] matrix1) {
        cells = new int [matrix1.length][matrix1[0].length];
        for (int i = 0; i< cells.length; i++) {
            fillCellsColumn(i, matrix1);
        }
    }

    public void fillCellsColumn (int i, int [][] matrix1) {
        for (int j = 0; j < cells[0].length; j++) {
            cells[i][j] = matrix1[i][j];
        }
    }

    /**
     * XI.2
     * @return o maior elemento da matriz.
     */

    public int highestValue() {
        int highest = getFirstElement();
        for (int i = 0; i< cells.length; i++) {
            highest = compareHighestValue(i, highest);
        }
        return highest;
    }

    public int compareHighestValue (int i, int highest) {
        for (int j = 0; j < cells[0].length; j++) {
            if (cells[i][j] > highest) {
                highest = cells[i][j];
            }
        }
        return highest;
    }

    /**
     * XI.3
     * @return o menor elemento da matriz.
     */

    public int lowestValue () {
        int lowest = getFirstElement();
        for (int i = 0; i< cells.length; i++) {
            lowest = compareLowestValues (i, lowest);
        }
        return lowest;
    }

    public int compareLowestValues (int i, int lowest) {
        for (int j = 0; j < cells[0].length; j++) {
            if (cells[i][j] < lowest) {
                lowest = cells[i][j];
            }
        }
        return lowest;
    }

    /**
     * XI.4
     * @return a média dos elementos da matriz.
     */

    public double ElemAverage () {
        int sum = 0;
        int count = 0;
        for (int i = 0; i < cells.length; i++) {
            sumElements(i, sum, count);
        }
        double average = (double) sum / count;
        return average;
    }

    public void sumElements (int i, int sum, int count) {
        for (int j = 0; j < cells[0].length; j++) {
            sum = sum + cells[i][j];
            count++;
        }
    }

    /**
     * XI.5
     * @return um vetor em que cada posição corresponde à soma dos elementos da linha homóloga da matriz.
     */

    public int[] somaLinhas() {
        int soma = 0;
        int [] vector = new int[cells.length];
        for (int i = 0; i < cells.length; i++) {
            soma = adicionarLinhas(i, soma);
            vector[i] = soma;
            soma = 0;
        }
        return vector;
    }

    public int adicionarLinhas (int i, int soma) {
        for (int j = 0; j < cells[0].length; j++) {
            soma = soma + cells[i][j];
        }
        return soma;
    }

    /**
     * XI.6
     * @return um vetor em que cada posição corresponde à soma dos elementos da coluna homóloga da matriz.
     */

    public int[] somaColunas() {
        int soma = 0;
        int[] vector = new int[cells[0].length];
        for (int i = 0; i < cells[0].length; i++) {
            soma = adicionarColunas(i, soma);
            vector[i] = soma;
            soma = 0;
        }
        return vector;
    }

    public int adicionarColunas (int i, int soma) {
        for (int j = 0; j < cells.length; j++) {
            soma = soma + cells[j][i];
        }
        return soma;
    }

    /**
     * XI.7
     * @return o índice da linha da matriz com maior soma dos respetivos elementos. Deve ser usado o método da alínea d).
     */

    public int indiceMaiorSoma() {
        if (cells.length == 0) {
            return -1;
        }
        int[] array = somaLinhas();
        int lnh = array.length;
        int ind = 0;
        for (int i = 0; i < lnh - 1; i++) {
            if (array[ind] < array[ind +1]) {
                ind = ind + 1;
            }
        }
        return ind;
    }

    /**
     * XI.8
     * @return True se a matriz é quadrada.
     */

    public boolean isSquareMatrix () {
        if (cells.length == 0) {
            return false;
        }
        if ( cells.length != cells[0].length) {
            return false;
        }
        return true;
    }

    /**
     * XI.9
     * @return True se a matriz quadrada é simétrica.
     */

    public boolean matrizQuadSim() {
        if (isSquareMatrix() == true) {
            for (int i = 0; i < cells.length; i++) {
                for (int j = 0; j < cells[0].length; j++) {
                    if (cells[i][j] != cells[j][i]) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * XI.10
     * @return a quantidade de elementos não nulos na diagonal principal da matriz (se quadrada) ou -1 (se não for
     * quadrada).
     */

    public int naoNulosDiagPrincQuadrada() {
        if (isSquareMatrix() == true) {
            int[] array = new int[cells.length];
            int conta = 0;
            for (int i = 0; i < cells.length; i++) {
                array[i] = cells[i][i];
                if (array[i] != 0) {
                    conta++;
                }
            }
            return conta;
        } else {
            return -1;
        }
    }

    /**
     * XI.11
     * @return True caso a diagonal principal e a secundária sejam iguais, i.e. tenham os mesmos elementos e pela mesma
     * ordem.
     */

    public boolean princIgualSecund() {
        if (isSquareMatrix() == true) {
            int lnh = cells.length;
            int col = cells[0].length;
            int ind1 = 0;
            for (int i = 0; i < lnh; i++) {
                for (int j = 0; j < col; j++) {
                    if (cells[ind1][ind1] != cells[ind1][col - 1]) {
                        return false;
                    }
                    if (cells[ind1][ind1] == cells[ind1][col - 1]) {
                        ind1++;
                        col--;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * XI.12
     * @return um vetor com todos os elementos da matriz cujo número de algarismos é superior número médio de algarismos
     * de todos os elementos da matriz.
     */

    public int somaAlgarismosVector () {
        int soma = 0;
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                int contaAlgarismos = FunctionLibrary.numAlgarismos(cells[i][j]);
                soma = soma + contaAlgarismos;
            }
        }
        return soma;
    }

    public double mediaAlgarismosVector() {
        int somaAlgarismos = somaAlgarismosVector();
        int nrNumerosArray = cells.length*cells[0].length;
        double media = (double) somaAlgarismos/nrNumerosArray;
        return media;
    }

    public int [] mediaSuperiorMediaAlgarismos () {
        if (cells.length == 0)
            return new int[0];
        double media = mediaAlgarismosVector();
        int [] vectorAlgSupMedia = new int [cells.length*cells[0].length];
        int countVectorAlgSupMedia = 0;
        int contaAlg;
        for (int i = 0; i< cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                contaAlg = FunctionLibrary.numAlgarismos(cells[i][j]);
                if (contaAlg > media) {
                    vectorAlgSupMedia [countVectorAlgSupMedia] = cells[i][j];
                    countVectorAlgSupMedia++;
                }
            }
        }
        if (vectorAlgSupMedia.length < 0) {
            return new int[0];
        }
        else {
            vectorAlgSupMedia = FunctionLibrary.copyNFirstElementArray(vectorAlgSupMedia, countVectorAlgSupMedia);
            return vectorAlgSupMedia;
        }
    }

    /**
     * XI.13
     * @return um vetor com todos os elementos da matriz cuja percentagem de algarismos pares é superior à média da
     * percentagem de algarismos pares de todos os elementos da matriz.
     */

    public double percentagemPares (long num) {
        long algPares = FunctionLibrary.numAlgarismosPares(num);
        long countAlgarismos = FunctionLibrary.numAlgarismos(num);
        double percPares = (double) algPares/countAlgarismos;
        return percPares;
    }

    public double mediaPercentagemPares () {
        double somaPercPares = 0;
        int contaNum = 0;
        for (int i = 0; i< cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                double percPares = percentagemPares(cells[i][j]);
                contaNum++;
                somaPercPares = somaPercPares + percPares;
            }
        }
        if (contaNum == 0) {
            return 0;
        }
        double mediaPercentPares = somaPercPares/contaNum;
        return mediaPercentPares;
    }

    public int [] percentParesSuperiorMedia () {
        if (cells.length == 0)
            return new int[0];
        double media = mediaPercentagemPares();
        int [] vectorAlgSupMedia = new int [cells.length*cells[0].length];
        int pos = 0;
        for (int i = 0; i< cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                if (percentagemPares(cells[i][j]) > media) {
                    vectorAlgSupMedia[pos] = cells[i][j];
                    pos++;
                }
            }
        }
        if (pos < 0) {
            return new int[0];
        }
        else {
            vectorAlgSupMedia = FunctionLibrary.copyNFirstElementArray(vectorAlgSupMedia, pos);
            return vectorAlgSupMedia;
        }
    }


    /**
     * XI.14 Metodo para multiplicar a matriz por um valor inteiro passado por parâmetro.
     * @param value
     */

    public void multiplyBy(int value) {
        for (int i = 0; i< cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                cells[i][j] = cells[i][j] * value;
            }
        }
    }

    /**
     * XI.15 Metodo para adicionar a matriz a uma Matriz passada por parâmetro.
     * @param matrix1
     */

    public void somaMatrizes(int[][] matrix1) {
        int lnh1 = cells.length;
        int col1 = cells[0].length;
        for (int i = 0; i < lnh1; i++) {
            cells = adicionarMatrizes(col1, i, matrix1);
        }
    }

    public int [][] adicionarMatrizes (int col1, int i, int [][] matrix1) {
        for (int j = 0; j < col1; j++) {
            cells[i][j] = cells[i][j] + matrix1[i][j];
        }
        return cells;
    }

    /**
     * XI.16 Metodo para multiplicar a matriz por outra Matriz passada por parâmetro.
     * @param matrix1
     */

    public void produtoMatrizes(int[][] matrix1) {
        int lnh1 = cells.length;
        int lnh2 = matrix1.length;
        int col2 = matrix1[0].length;
        int soma = 0;
        int [][] matrix2 = copyElementsMatrix(cells, cells.length, cells[0].length);
        for (int i = 0; i < lnh1; i++) {
            cells = somaMatrizes(col2, soma, lnh2, i, matrix1, matrix2);
        }
    }

    public int [][] somaMatrizes (int col2, int soma, int lnh2, int i, int [][] matrix1, int [][] matrix2) {
        for (int j = 0; j < col2; j++) {
            soma = multicarMatrizes(lnh2, soma, i, j, matrix1, matrix2);
            cells[i][j] = soma;
            soma = 0;
        }
        return cells;
    }

    public int multicarMatrizes (int lnh2, int soma, int i, int j, int [][] matrix1, int [][] matrix2) {
        for (int k = 0; k < lnh2; k++) { // o k vai dar o indice para fazer a adição da multiplicação em multiplicacão de matrizes,
            soma = soma + matrix2[i][k] * matrix1[k][j]; //numa linha corre as colunas necessárias;
        }
        return soma;
    }

    public int[][] copyElementsMatrix(int[][] matrix, int lines, int columns){
        int [][] newMatrix = new int[lines][columns];

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < columns; j++) {
                newMatrix[i][j] = matrix[i][j];
            }
        }
        return newMatrix;
    }

    /**
     * XI.17 Metodo para inverter a ordem dos valores de cada linha da matriz.
     */

    public void invertLineOrder () {
        int [][] arrayAux = new int [cells.length][cells[0].length];
        for (int i = 0; i < cells.length; i++) {
            arrayAux = invertOrder(i, arrayAux);
        }
        cells = copyElementsMatrix(arrayAux, arrayAux.length, arrayAux[0].length);
    }

    public int [][] invertOrder (int i, int [][] arrayAux) {
        for (int k = cells[0].length-1, j = 0; k > -1; k--, j++) {
            arrayAux[i][j] = cells[i][k];
        }
        return arrayAux;
    }

    /**
     * XI.18 Metodo para inverter a ordem dos valores de cada coluna da matriz.
     */

    public void invertColumnOrder () {
        int [][] arrayAux = new int [cells.length][cells[0].length];
        for (int i = 0; i < cells[0].length; i++) {
            arrayAux = invertOrder2(i, arrayAux);
        }
        cells = copyElementsMatrix(arrayAux, arrayAux.length, arrayAux[0].length);
    }

    public int [][] invertOrder2 (int i, int [][] arrayAux) {
        for (int k = cells.length-1, j = 0; k > -1; k--, j++) {
            arrayAux[j][i] = cells[k][i];
        }
        return arrayAux;
    }

    /**
     * XI.19 Metodo para rodar 90º os valores da matriz.
     */

    public void rotate90Degrees () {
        if ( cells.length == cells[0].length) {
            int linha = cells.length;
            for (int i = 0; i < linha/2; i++) {
                for (int j = 0; j < linha - i - 1; j++) {
                    int temp = cells[i][j];
                    cells[i][j] = cells[linha - 1 - j][i];
                    cells[linha - 1 - j][i] = cells[linha - 1 - i][linha - 1 - j];
                    cells[linha - 1 - i][linha - 1 - j] = cells[j][linha - 1 - i];
                    cells[j][linha - 1 - i] = temp;

                }
            }
        }
        if (cells.length != cells[0].length && (cells.length != 0 && cells[0].length !=0)) {
            int [][] matrix2 = new int [cells[0].length][cells.length];
            int lin = cells.length;
            int col = cells[0].length;
            for (int i = 0; i < col; i++) {
                for (int j = 0, k = lin-1; j < lin; j++, k--) {
                    matrix2[i][j] = cells[k][i];
                }
            }
            cells = matrix2;
        }
    }

    /**
     * XI.20 Metodo para rodar 180º os valores da matriz.
     */

    public void rotate180Degrees () {
        rotate90Degrees();
        rotate90Degrees();
    }


    /**
     * XI.21 Metodo para rodar -90º os valores da matriz.
     */

    public void rotateNeg90Degrees () {
        if (cells.length == cells[0].length) {
            rotate90Degrees();
            rotate90Degrees();
            rotate90Degrees();
        }
    }

}
