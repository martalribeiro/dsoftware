package Extra;

public class Address {

    private String line1;
    private String line2;


    public Address(String line1, String line2) {
        this.line1 = line1;
        this.line2 = line2;
    }

    public void setLine1(String l1) {
        this.line1 = l1;
    }

    public void setLine2(String l2) {
        this.line2 = l2;
    }

}
