package Extra;

import java.util.Objects;

public class Person {

    private String name; // all are private because they are private info that no one can access
    private Address myAddress;
    private float height;
    private float weight;

    public Person(String name, Address adress, float height, float weight) {
        this.name = name;
        this.myAddress = adress;
        this.height = height;
        this.weight = weight;
    }

    public void setName() {
    }

    public Address getMyAdress(String l1, String l2) {
        myAddress = new Address(l1, l2);
        return myAddress;
    }

    public void settHeight() {
    }

    public void setWeight() {
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }
        Person people = (Person) o;
        return Float.compare(people.height, height) == 0 &&
                Float.compare(people.weight, weight) == 0 &&
                Objects.equals(name, people.name) &&
                Objects.equals(myAddress, people.myAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, myAddress, height, weight);
    }
}
